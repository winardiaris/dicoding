package xyz.winardiaris.android.mynotesapp.databases;

import android.provider.BaseColumns;

/**
 * Created by winardiaris on 15/11/17.
 */

public class DatabaseContract {
  static String TABLE_NOTE = "note";
  static final class NoteColumns implements BaseColumns {
    //Note title
    static String TITLE = "title";
    //Note description
    static String DESCRIPTION = "description";
    //Note date
    static String DATE = "date";
  }
}
