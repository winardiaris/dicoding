package xyz.winardiaris.android.mypreloaddata.databases;

import android.provider.BaseColumns;

/**
 * Created by winardiaris on 15/11/17.
 */

public class DatabaseContract {
  public static String TABLE_NAME = "table_mahasiswa";

  public static final class MahasiswaColumns implements BaseColumns {

    // Mahasiswa nama
    public static String NAMA = "nama";
    // Mahasiswa nim
    public static String NIM = "nim";

  }

}
