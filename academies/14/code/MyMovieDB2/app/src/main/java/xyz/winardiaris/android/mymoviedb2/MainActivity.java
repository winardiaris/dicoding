package xyz.winardiaris.android.mymoviedb2;


import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {
  CircleImageView circleImageView;
  static  String profilePicture = "https://scontent.fjkt1-1.fna.fbcdn.net/v/t1.0-9/19554780_1493122320711100_3911771924510389153_n.jpg?oh=71b03a7a698b4b45287d7ea6ace6ac6e&oe=5A91DD2E";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    circleImageView = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);
    Glide.with(MainActivity.this)
        .load(profilePicture)
        .into(circleImageView);

    initFragmentManager();
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    SearchView searchView =
        (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
    searchView.setQueryHint(getResources().getString(R.string.type_search));
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

      @Override
      public boolean onQueryTextSubmit(String query) {
        Toast.makeText(MainActivity.this,query,Toast.LENGTH_SHORT).show();

        searchShow(query);
        return true;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        return false;
      }
    });
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();


    return super.onOptionsItemSelected(item);
  }

  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_now_playing) {
      nowPlayingShow();
    } else if (id == R.id.nav_upcoming) {
      upcomingShow();
    } else if (id == R.id.nav_setting) {
      Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
      startActivity(mIntent);
    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  public void initFragmentManager() {
    nowPlayingShow();
  }

  public void nowPlayingShow() {
    Fragment fragment                 = null;
    fragment                          = new NowPlayingFragment();
    FragmentManager manager           = getSupportFragmentManager();
    FragmentTransaction transaction   = manager.beginTransaction();
    transaction.replace(R.id.frame_container, fragment);
    transaction.commit();

    String title = getResources().getString(R.string.now_playing_movies);
    getSupportActionBar().setTitle(title);
  }

  public void upcomingShow() {
    Fragment fragment                 = null;
    fragment                          = new UpcomingFragment();
    FragmentManager manager           = getSupportFragmentManager();
    FragmentTransaction transaction   = manager.beginTransaction();

    transaction.replace(R.id.frame_container, fragment);
    transaction.addToBackStack(null);
    transaction.commit();

    String title = getResources().getString(R.string.upcoming_movies);
    getSupportActionBar().setTitle(title);
  }
  public void searchShow(String search) {
    Fragment fragment                 = null;
    fragment                          = new SearchFragment(search);
    FragmentManager manager           = getSupportFragmentManager();
    FragmentTransaction transaction   = manager.beginTransaction();

    Bundle mBundle = new Bundle();
    mBundle.putString(SearchFragment.EXTRA_SEARCH, search);

    transaction.replace(R.id.frame_container, fragment);
    transaction.addToBackStack(null);
    transaction.commit();

    getSupportActionBar().setTitle(search);
  }

}
