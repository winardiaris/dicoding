package xyz.winardiaris.android.mymoviedb2;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by winardiaris on 23/10/17.
 */

public class Utils {
  public String humanReadableDate(String dateInString) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
    try {
      Date date = formatter.parse(dateInString);
      formatter.applyPattern("E, MMM dd yyyy");
      return formatter.format(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public String getSafeSubstring(String s, int maxLength){
    if(!TextUtils.isEmpty(s)){
      if(s.length() >= maxLength){
        return s.substring(0, maxLength) + "...";
      }
    }
    return s;
  }
}
