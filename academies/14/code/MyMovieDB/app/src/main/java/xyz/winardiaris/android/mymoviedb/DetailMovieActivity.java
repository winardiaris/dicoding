package xyz.winardiaris.android.mymoviedb;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class DetailMovieActivity extends AppCompatActivity {
  private static final String TAG             = "DetailMovieActivity";
  private static final String EXTRAS_MOVIE_ID = "EXTRAS_MOVIE_ID";
  private static final String API_KEY         = BuildConfig.API_KEY;

  String movie_id       = "";
  String jsonString     = "";

  DetailMovieActivity   detailMovieActivity;
  Context               context;
  ImageView             movie_poster;
  ProgressBar           spinner;
  RelativeLayout        detail_movie;
  RelativeLayout        progress_bar;
  TextView              movie_title;
  TextView              movie_tagline;
  TextView              movie_overview;
  TextView              movie_release_date;
  TextView              movie_vote;
  TextView              movie_lang;
  TextView              movie_production_companies;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_movie);

    Intent intent = getIntent();
    this.detail_movie               = (RelativeLayout) findViewById(R.id.detail_movie);
    this.movie_id                   = intent.getStringExtra(EXTRAS_MOVIE_ID);
    this.movie_lang                 = (TextView) findViewById(R.id.movie_lang);
    this.movie_overview             = (TextView) findViewById(R.id.movie_overview);
    this.movie_release_date         = (TextView) findViewById(R.id.movie_release_date);
    this.movie_tagline              = (TextView) findViewById(R.id.movie_tagline);
    this.movie_title                = (TextView) findViewById(R.id.movie_title);
    this.movie_poster               = (ImageView) findViewById(R.id.movie_poster);
    this.movie_production_companies = (TextView) findViewById(R.id.movie_production_companies);
    this.movie_vote                 = (TextView) findViewById(R.id.movie_vote);
    this.progress_bar               = (RelativeLayout) findViewById(R.id.progress_bar);
    this.spinner                    = (ProgressBar)findViewById(R.id.progressBar);

    GetDetailMovie getDetailMovie = new GetDetailMovie();
    getDetailMovie.execute();
  }

  private void hide() {
    detail_movie.setVisibility(View.GONE);
    progress_bar.setVisibility(View.VISIBLE);
  }

  private void show() {
    detail_movie.setVisibility(View.VISIBLE);
    progress_bar.setVisibility(View.GONE);
  }

  private class GetDetailMovie extends AsyncTask<String, Void, String> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Log.d(TAG, "onPreExecute: start");
      hide();
    }

    @Override
    protected String doInBackground(String... params) {
      Log.d(TAG, "doInBackground: start");
      SyncHttpClient client = new SyncHttpClient();

      String URL = BuildConfig.BASE_URL + "movie/" + movie_id +"?api_key=" + API_KEY;
      client.get(URL, new AsyncHttpResponseHandler() {
        @Override
        public void onStart() {
          super.onStart();
          setUseSynchronousMode(true);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
          try {
            String result = new String(responseBody);
            jsonString = result;
          } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onSuccess: request fail", e);
          }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

        }
      });
      return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      Utils utils = new Utils();

      JSONObject responseObject = null;
      try {
        responseObject = new JSONObject(s);

        String posterUrl          = "";
        String responseCompanies  = "";
        String responseLang       = responseObject.getString("original_language");
        String responseOverview   = responseObject.getString("overview");
        String responsePoster     = responseObject.getString("poster_path");
        String responseRelease    = responseObject.getString("release_date");
        String responseTagline    = responseObject.getString("tagline");
        String responseTitle      = responseObject.getString("original_title");
        String responseVote       = responseObject.getString("vote_average");


        JSONArray list = responseObject.getJSONArray("production_companies");

        for (int i = 0 ; i < list.length() ; i++) {
          JSONObject company = list.getJSONObject(i);
          responseCompanies += company.getString("name") + ", ";
        }

        if(responsePoster == "null") {
          posterUrl = BuildConfig.DEFAULT_POSTER;
        } else {
          posterUrl = BuildConfig.POSTER_URL + "w780" + responsePoster;
        }

        movie_lang.setText(responseLang.toUpperCase());
        movie_overview.setText(responseOverview);
        movie_production_companies.setText(responseCompanies);
        movie_release_date.setText(utils.humanReadableDate(responseRelease));
        movie_tagline.setText(responseTagline);
        movie_title.setText(responseTitle);
        movie_vote.setText(responseVote);

        Picasso.with(context).load(posterUrl).into(movie_poster);

      } catch (JSONException e) {
        e.printStackTrace();
      }

      show();
    }
  }
}
