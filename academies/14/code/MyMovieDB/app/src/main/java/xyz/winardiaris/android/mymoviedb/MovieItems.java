package xyz.winardiaris.android.mymoviedb;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by winardiaris on 23/10/17.
 */

public class MovieItems {
  private int id;
  private String title;
  private String overview;
  private String poster;
  private String release_date;

  public  MovieItems(JSONObject object) {
    try {
      int id                = object.getInt("id");
      String title          = object.getString("title");
      String overview       = object.getString("overview");
      String release_date   = object.getString("release_date");
      String poster_path    = object.getString("poster_path");
      String poster_url     = "";

      if(poster_path == "null") {
        poster_url = BuildConfig.DEFAULT_POSTER;
      } else {
        poster_url = BuildConfig.POSTER_URL + "w342" + poster_path;
      }

      this.id           = id;
      this.overview     = overview;
      this.poster       = poster_url;
      this.release_date = release_date;
      this.title        = title;

    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public String getOverview() {
    return overview;
  }

  public String getPoster() {
    return poster;
  }

  public String getRelease_date() {
    return release_date;
  }
}
