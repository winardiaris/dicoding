package xyz.winardiaris.android.mymoviedb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by winardiaris on 23/10/17.
 */

public class MovieAdapter extends BaseAdapter {
  private ArrayList<MovieItems> arrayListMovie = new ArrayList<>();
  private LayoutInflater layoutInflater;
  private Context context;
  private String urlConfig;

  public MovieAdapter(Context context){
    this.context    = context;
    layoutInflater  = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  public void setData(ArrayList<MovieItems> items){
    arrayListMovie  = items;
    notifyDataSetChanged();
  }

  @Override
  public int getCount() {
    return arrayListMovie.size();
  }

  @Override
  public MovieItems getItem(int position) {
    return arrayListMovie.get(position);
  }

  @Override
  public long getItemId(int position) {
    return  position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder = null;
    Utils utils       = new Utils();

    if(convertView == null) {
      holder                = new ViewHolder();
      convertView           = layoutInflater.inflate(R.layout.movie_list, null);
      holder.tvTitle        = convertView.findViewById(R.id.movie_title);
      holder.tvOverview     = convertView.findViewById(R.id.movie_overview);
      holder.tvReleaseDate  = convertView.findViewById(R.id.movie_release_date);
      holder.ivPoster       = convertView.findViewById(R.id.movie_poster);
      convertView.setTag(holder);
    } else {
      holder                = (ViewHolder) convertView.getTag();
    }

    String release_date     = arrayListMovie.get(position).getRelease_date();

    holder.tvOverview.setText(arrayListMovie.get(position).getOverview());
    holder.tvReleaseDate.setText(utils.humanReadableDate(release_date));
    holder.tvTitle.setText(arrayListMovie.get(position).getTitle());

    Picasso.with(context).load(arrayListMovie.get(position).getPoster()).into(holder.ivPoster);

    return convertView;
  }

  public static class ViewHolder {
    public TextView   tvTitle;
    public TextView   tvOverview;
    public TextView   tvReleaseDate;
    public ImageView  ivPoster;
  }
}
