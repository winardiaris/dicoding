package xyz.winardiaris.android.mymoviedb;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by winardiaris on 23/10/17.
 */

public class MovieAsyncTaskLoader extends AsyncTaskLoader<ArrayList<MovieItems>>{
  private static final String TAG = "MovieAsyncTaskLoader";
  private static final String API_KEY = BuildConfig.API_KEY;

  private ArrayList<MovieItems> mData;
  private boolean hasResult = true;
  private String search ;

  public MovieAsyncTaskLoader(Context context, String search) {
    super(context);

    onContentChanged();
    this.search = search;
  }

  @Override
  protected void onStartLoading() {
    if (takeContentChanged())
      forceLoad();
    else if (hasResult)
      deliverResult(mData);
  }

  @Override
  public void deliverResult(final ArrayList<MovieItems> data) {
    mData = data;
    hasResult = true;
    super.deliverResult(data);
  }

  @Override
  protected void onReset() {
    super.onReset();
    onStopLoading();
    if (hasResult) {
      onReleaseResources(mData);
      mData = null;
      hasResult = false;
    }
  }

  private void onReleaseResources(ArrayList<MovieItems> mData) {
    Log.d(TAG, "onReleaseResources: " + mData);
  }

  @Override
  public ArrayList<MovieItems> loadInBackground() {
    SyncHttpClient client = new SyncHttpClient();
    String URL = BuildConfig.BASE_URL + "search/movie?api_key=" + API_KEY + "&language=en-US&query=" + search;

    final ArrayList<MovieItems> movieItemses = new ArrayList<>();
    client.get(URL, new AsyncHttpResponseHandler() {
      @Override
      public void onStart() {
        super.onStart();
        setUseSynchronousMode(true);
      }

      @Override
      public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
          String result             = new String(responseBody);
          JSONObject responseObject = new JSONObject(result);
          JSONArray list            = responseObject.getJSONArray("results");

          for (int i = 0 ; i < list.length() ; i++) {
            JSONObject movie        = list.getJSONObject(i);
            MovieItems movieItems   = new MovieItems(movie);
            movieItemses.add(movieItems);
          }

        } catch (Exception e) {
          e.printStackTrace();
          Log.e(TAG, "onSuccess: request fail", e);
        }
      }

      @Override
      public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

      }
    });
      return movieItemses;
  }

  public ArrayList<MovieItems> getResult() {
    return mData;
  }
}
