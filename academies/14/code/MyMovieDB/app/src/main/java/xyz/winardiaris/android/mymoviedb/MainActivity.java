package xyz.winardiaris.android.mymoviedb;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
    LoaderManager.LoaderCallbacks<ArrayList<MovieItems>>,
    AdapterView.OnItemClickListener{
  private static final String TAG = "MainActivity";
  private static final String EXTRAS_SEARCH = "EXTRAS_SEARCH";

  EditText      searchMovie;
  Button        searchButton;
  ListView      listView;
  MovieAdapter  adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    adapter       = new MovieAdapter(this);
    listView      = (ListView) findViewById(R.id.lv_movie);
    searchMovie   = (EditText) findViewById(R.id.edt_search);

    adapter.notifyDataSetChanged();

    listView.setAdapter(adapter);
    listView.setOnItemClickListener(this);

    searchButton  = (Button) findViewById(R.id.btn_search);
    searchButton.setOnClickListener(searchListener);

    String search = searchMovie.getText().toString();

    Bundle bundle = new Bundle();
    bundle.putString(EXTRAS_SEARCH,search);
    getLoaderManager().initLoader(0, bundle, this);
  }

  View.OnClickListener searchListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      String movie = searchMovie.getText().toString();
      if (TextUtils.isEmpty(movie))return;

      Bundle bundle = new Bundle();
      bundle.putString(EXTRAS_SEARCH,movie);
      getLoaderManager().restartLoader(0,bundle,MainActivity.this);
    }
  };

  @Override
  public Loader<ArrayList<MovieItems>> onCreateLoader(int id, Bundle args) {
    Log.d("Create loader","1");
    String movie = "";
    if (args != null){
      movie = args.getString(EXTRAS_SEARCH);
    }

    return new MovieAsyncTaskLoader(this, movie);
  }

  @Override
  public void onLoadFinished(Loader<ArrayList<MovieItems>> loader, ArrayList<MovieItems> data) {
    Log.d("Load Finish","1");
    adapter.setData(data);
  }

  @Override
  public void onLoaderReset(Loader<ArrayList<MovieItems>> loader) {
    Log.d("Load Reset","1");
  }

  @Override
  public void onBackPressed(){
    super.onBackPressed();
    finish();
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Intent intent = new Intent(this, DetailMovieActivity.class);

    String movie_id = String.valueOf(adapter.getItem(position).getId());
    intent.putExtra("EXTRAS_MOVIE_ID",movie_id);
    startActivity(intent);
  }
}
