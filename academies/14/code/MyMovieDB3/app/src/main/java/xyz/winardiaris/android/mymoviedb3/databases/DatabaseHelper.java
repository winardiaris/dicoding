package xyz.winardiaris.android.mymoviedb3.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.*;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.TABLE_NAME;

/**
 * Created by winardiaris on 21/11/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
  public static String      DATABASE_NAME       = "moviedb";
  private static final int  DATABASE_VERSION    = 1;

  private static final String SQL_CREATE_TABLE_FAVORITE = String.format("CREATE TABLE IF NOT EXISTS %s"
          + " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
          " %s TEXT NOT NULL," +
          " %s TEXT NOT NULL," +
          " %s TEXT NOT NULL," +
          " %s TEXT NOT NULL," +
          " %s TEXT NOT NULL," +
          " %s TEXT," +
          " %s TEXT," +
          " %s TEXT," +
          " %s TEXT," +
          " %s TEXT," +
          "UNIQUE (%s) ON CONFLICT REPLACE)",
      TABLE_NAME,
      _ID,
      MOVIE_ID,
      TITLE,
      OVERVIEW,
      POSTER,
      RELEASE_DATE,
      TAGLINE,
      ORIGINAL_LANGUAGE,
      ORIGINAL_TITLE,
      VOTE_AVERAGE,
      PRODUCTION_COMPANIES,
      MOVIE_ID
  );



  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(SQL_CREATE_TABLE_FAVORITE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }
}
