package xyz.winardiaris.android.mymoviedb3.utils;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import xyz.winardiaris.android.mymoviedb3.BuildConfig;
import xyz.winardiaris.android.mymoviedb3.model.Movie;
import xyz.winardiaris.android.mymoviedb3.databases.FavoriteHelper;

import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.CONTENT_URI;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.MOVIE_ID;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.ORIGINAL_LANGUAGE;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.ORIGINAL_TITLE;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.OVERVIEW;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.POSTER;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.PRODUCTION_COMPANIES;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.RELEASE_DATE;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.TAGLINE;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.TITLE;
import static xyz.winardiaris.android.mymoviedb3.databases.FavoriteContract.MovieColumns.VOTE_AVERAGE;

/**
 * Created by winardiaris on 23/10/17.
 */

public class Utils {
  String          jsonString;
  FavoriteHelper  favoriteHelper;

  public String humanReadableDate(String dateInString) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
    try {
      Date date = formatter.parse(dateInString);
      formatter.applyPattern("E, MMM dd yyyy");
      return formatter.format(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public String getSafeSubstring(String s, int maxLength){
    if(!TextUtils.isEmpty(s)){
      if(s.length() >= maxLength){
        return s.substring(0, maxLength) + "...";
      }
    }
    return s;
  }

  public String getJsonString(String url) {
    SyncHttpClient client = new SyncHttpClient();
    client.get(url, new AsyncHttpResponseHandler() {
      @Override
      public void onStart() {
        super.onStart();
        setUseSynchronousMode(true);
      }

      @Override
      public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
          jsonString =  new String(responseBody);
        } catch (Exception e) {
          e.printStackTrace();
          Log.e("getJsonString", "onSuccess: request fail", e);
        }
      }

      @Override
      public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

      }
    });
    return jsonString;
  }

  public void setFavorite(Context context, Movie movie) {
    Log.d("UTILS", "setFavorite: " + movie.getTitle());
    ContentValues contentValues = convertToContentValues(movie);
    context.getContentResolver().insert(CONTENT_URI, contentValues);
  }
  public void setDelete(Context context, Movie movie) {
    Log.d("UTILS", "delete: " + movie.getTitle());
    favoriteHelper = new FavoriteHelper(context);
    favoriteHelper.open();
    favoriteHelper.delete(movie.getId());
    favoriteHelper.close();
  }

  public ContentValues convertToContentValues(Movie movie) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(MOVIE_ID, movie.getMovie_id());
    contentValues.put(TITLE, movie.getTitle());
    contentValues.put(OVERVIEW, movie.getOverview());
    contentValues.put(POSTER, movie.getPoster());
    contentValues.put(RELEASE_DATE, movie.getRelease_date());
    contentValues.put(TAGLINE, movie.getTagline());
    contentValues.put(ORIGINAL_LANGUAGE, movie.getOriginal_language());
    contentValues.put(ORIGINAL_TITLE, movie.getOriginal_title());
    contentValues.put(VOTE_AVERAGE, movie.getVote_average());
    contentValues.put(PRODUCTION_COMPANIES, movie.getProduction_companies());

    return contentValues;
  }
}
