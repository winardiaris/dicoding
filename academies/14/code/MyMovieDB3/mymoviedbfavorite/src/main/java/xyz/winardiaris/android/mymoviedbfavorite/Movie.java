package xyz.winardiaris.android.mymoviedbfavorite;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.provider.BaseColumns._ID;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.MOVIE_ID;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.ORIGINAL_LANGUAGE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.ORIGINAL_TITLE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.OVERVIEW;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.POSTER;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.PRODUCTION_COMPANIES;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.RELEASE_DATE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.TAGLINE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.TITLE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.VOTE_AVERAGE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.getColumnInt;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.getColumnString;

/**
 * Created by winardiaris on 23/10/17.
 */

public class Movie implements Parcelable {
  private int id;
  private int movie_id;
  private String title;
  private String overview;
  private String poster;
  private String release_date;
  private String tagline;
  private String original_language;
  private String original_title;
  private String vote_average;
  private String production_companies;

  public Movie() {
  }

  public Movie(JSONObject object) {
    try {
      int id                = object.getInt("id");
      String title          = object.getString("title");
      String overview       = object.getString("overview");
      String release_date   = object.getString("release_date");
      String poster_path    = object.getString("poster_path");
      String poster_url     = "";

      if(poster_path == "null") {
        poster_url = BuildConfig.DEFAULT_POSTER;
      } else {
        poster_url = BuildConfig.POSTER_URL + "w342" + poster_path;
      }

      this.id           = id;
      this.movie_id     = id;
      this.overview     = overview;
      this.poster       = poster_url;
      this.release_date = release_date;
      this.title        = title;

    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  protected Movie(Parcel in) {
    id = in.readInt();
    movie_id = in.readInt();
    title = in.readString();
    overview = in.readString();
    poster = in.readString();
    release_date = in.readString();
    tagline = in.readString();
    original_language = in.readString();
    original_title = in.readString();
    vote_average = in.readString();
    production_companies = in.readString();
  }

  public static final Creator<Movie> CREATOR = new Creator<Movie>() {
    @Override
    public Movie createFromParcel(Parcel in) {
      return new Movie(in);
    }

    @Override
    public Movie[] newArray(int size) {
      return new Movie[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeInt(movie_id);
    dest.writeString(title);
    dest.writeString(overview);
    dest.writeString(poster);
    dest.writeString(release_date);
    dest.writeString(tagline);
    dest.writeString(original_language);
    dest.writeString(original_title);
    dest.writeString(vote_average);
    dest.writeString(production_companies);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getMovie_id() {
    return movie_id;
  }

  public void setMovie_id(int movie_id) {
    this.movie_id = movie_id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getOverview() {
    return overview;
  }

  public void setOverview(String overview) {
    this.overview = overview;
  }

  public String getPoster() {
    return poster;
  }

  public void setPoster(String poster) {
    this.poster = poster;
  }

  public String getRelease_date() {
    return release_date;
  }

  public void setRelease_date(String release_date) {
    this.release_date = release_date;
  }

  public String getTagline() {
    return tagline;
  }

  public void setTagline(String tagline) {
    this.tagline = tagline;
  }

  public String getOriginal_language() {
    return original_language;
  }

  public void setOriginal_language(String original_language) {
    this.original_language = original_language;
  }

  public String getOriginal_title() {
    return original_title;
  }

  public void setOriginal_title(String original_title) {
    this.original_title = original_title;
  }

  public String getVote_average() {
    return vote_average;
  }

  public void setVote_average(String vote_average) {
    this.vote_average = vote_average;
  }

  public String getProduction_companies() {
    return production_companies;
  }

  public void setProduction_companies(String production_companies) {
    this.production_companies = production_companies;
  }

  public Movie(String json) {
    JSONObject responseObject = null;
    try {
      responseObject = new JSONObject(json);
      String posterUrl          = "";
      String responseCompanies  = "";
      int responseid            = responseObject.getInt("id");
      String responseLang       = responseObject.getString("original_language");
      String responseOtitle       = responseObject.getString("original_title");
      String responseOverview   = responseObject.getString("overview");
      String responsePoster     = responseObject.getString("poster_path");
      String responseRelease    = responseObject.getString("release_date");
      String responseTagline    = responseObject.getString("tagline");
      String responseTitle      = responseObject.getString("original_title");
      String responseVote       = responseObject.getString("vote_average");


      JSONArray list = responseObject.getJSONArray("production_companies");

      for (int i = 0 ; i < list.length() ; i++) {
        JSONObject company = list.getJSONObject(i);
        responseCompanies += company.getString("name") + ", ";
      }

      if(responsePoster == "null") {
        posterUrl = BuildConfig.DEFAULT_POSTER;
      } else {
        posterUrl = BuildConfig.POSTER_URL + "w780" + responsePoster;
      }

      this.movie_id             = responseid;
      this.title                = responseTitle;
      this.overview             = responseOverview;
      this.poster               = posterUrl;
      this.release_date         = responseRelease;
      this.original_language    = responseLang;
      this.original_title       = responseOtitle;
      this.tagline              = responseTagline;
      this.vote_average         = responseVote;
      this.production_companies = responseCompanies;

    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  public Movie(Cursor cursor) {
    this.id                   = getColumnInt(cursor, _ID);;
    this.movie_id             = getColumnInt(cursor, MOVIE_ID);
    this.title                = getColumnString(cursor, TITLE);
    this.overview             = getColumnString(cursor, OVERVIEW);
    this.poster               = getColumnString(cursor, POSTER);
    this.release_date         = getColumnString(cursor, RELEASE_DATE);
    this.original_language    = getColumnString(cursor, ORIGINAL_LANGUAGE);
    this.original_title       = getColumnString(cursor, ORIGINAL_TITLE);
    this.tagline              = getColumnString(cursor, TAGLINE);
    this.vote_average         = getColumnString(cursor, VOTE_AVERAGE);
    this.production_companies = getColumnString(cursor, PRODUCTION_COMPANIES);
  }
}
