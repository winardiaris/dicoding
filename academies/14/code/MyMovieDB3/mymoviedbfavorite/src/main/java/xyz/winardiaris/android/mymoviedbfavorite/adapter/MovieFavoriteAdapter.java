package xyz.winardiaris.android.mymoviedbfavorite.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import xyz.winardiaris.android.mymoviedbfavorite.R;

import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.OVERVIEW;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.POSTER;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.RELEASE_DATE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.TITLE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.getColumnString;

/**
 * Created by winardiaris on 25/11/17.
 */

public class MovieFavoriteAdapter extends CursorAdapter {
  public MovieFavoriteAdapter(Context context, Cursor c, boolean autoRequery) {
    super(context, c, autoRequery);
  }

  @Override
  public View newView(Context context, Cursor cursor, ViewGroup parent) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_favorite, parent, false);
    return view;
  }

  @Override
  public void bindView(View view, Context context, Cursor cursor) {
    if (cursor != null){
      ImageView movie_poster = view.findViewById(R.id.movie_poster);
      TextView movie_title = view.findViewById(R.id.movie_title);
      TextView movie_overview = view.findViewById(R.id.movie_overview);
      TextView movie_release_date = view.findViewById(R.id.movie_release_date);


      Glide.with(context)
          .load(getColumnString(cursor, POSTER))
          .override(300,400)
          .into(movie_poster);

      movie_title.setText(getColumnString(cursor,TITLE));
      movie_overview.setText(getColumnString(cursor,OVERVIEW));
      movie_release_date.setText(getColumnString(cursor,RELEASE_DATE));
    }
  }

  @Override
  public Cursor getCursor() {
    return super.getCursor();
  }
}
