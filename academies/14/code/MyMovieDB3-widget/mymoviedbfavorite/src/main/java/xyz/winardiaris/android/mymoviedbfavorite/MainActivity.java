package xyz.winardiaris.android.mymoviedbfavorite;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import xyz.winardiaris.android.mymoviedbfavorite.adapter.MovieFavoriteAdapter;

import static android.provider.BaseColumns._ID;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.CONTENT_URI;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.MovieColumns.TITLE;
import static xyz.winardiaris.android.mymoviedbfavorite.databases.FavoriteContract.getColumnString;

public class MainActivity extends AppCompatActivity implements
    LoaderManager.LoaderCallbacks<Cursor>,
    AdapterView.OnItemClickListener {
  private MovieFavoriteAdapter movieFavoriteAdapter;
  ListView listView;
  private final int LOAD_NOTES_ID = 110;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    getSupportActionBar().setTitle("Movie Favorite");

    listView = (ListView)findViewById(R.id.lv_notes);
    movieFavoriteAdapter = new MovieFavoriteAdapter(this, null, true);
    listView.setAdapter(movieFavoriteAdapter);
    listView.setOnItemClickListener(this);

    getSupportLoaderManager().initLoader(LOAD_NOTES_ID, null, this);

  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    return new CursorLoader(this, CONTENT_URI, null, null, null, null);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    movieFavoriteAdapter.swapCursor(data);
  }

  @Override
  protected void onPostResume() {
    super.onPostResume();
    getSupportLoaderManager().restartLoader(LOAD_NOTES_ID, null, this);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    movieFavoriteAdapter.swapCursor(null);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    getSupportLoaderManager().destroyLoader(LOAD_NOTES_ID);
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Cursor cursor = (Cursor) movieFavoriteAdapter.getItem(position);
    Toast.makeText(getApplicationContext(), getColumnString(cursor,TITLE), Toast.LENGTH_SHORT).show();
  }

}
