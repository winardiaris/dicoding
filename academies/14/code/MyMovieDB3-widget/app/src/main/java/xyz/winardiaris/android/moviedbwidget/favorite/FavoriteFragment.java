package xyz.winardiaris.android.moviedbwidget.favorite;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import xyz.winardiaris.android.moviedbwidget.DetailMovieActivity;
import xyz.winardiaris.android.moviedbwidget.model.Movie;
import xyz.winardiaris.android.moviedbwidget.R;
import xyz.winardiaris.android.moviedbwidget.databases.FavoriteHelper;

import static xyz.winardiaris.android.moviedbwidget.DetailMovieActivity.EXTRAS_DELETE;
import static xyz.winardiaris.android.moviedbwidget.DetailMovieActivity.EXTRAS_MOVIE_ID;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.CONTENT_URI;

public class FavoriteFragment extends Fragment {
  private static final String TAG     = FavoriteFragment.class.getSimpleName();
  protected View              view;
  protected RecyclerView      recyclerView;
//  private ArrayList<Movie>    list;
  private Cursor              list;
  private RelativeLayout      loading;
  private FavoriteHelper      favoriteHelper;
  private SwipeRefreshLayout  swipeRefreshLayout;


  public FavoriteFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.fragment_favorite, container, false);
    view.setTag(TAG);
    this.loading = view.findViewById(R.id.loading);
    swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        loadData();
      }
    });

    favoriteHelper = new FavoriteHelper(this.getContext());
    favoriteHelper.open();
    loadData();
    return view;
  }


  private void loadData() {
    AsyncTask<Void, Void, Cursor> task = new AsyncTask<Void, Void, Cursor>() {
      @Override
      protected Cursor doInBackground(Void... voids) {
        Log.d(TAG, "doInBackground: start");
          return getContext().getContentResolver().query(CONTENT_URI,null,null,null,null);
      }

      @Override
      protected void onPostExecute(final Cursor movies) {
        list = movies;
        recyclerView = view.findViewById(R.id.rv_favorite);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        MovieAdapterFavorite movieAdapterFavorite = new MovieAdapterFavorite(getActivity());
        movieAdapterFavorite.setListMovie(list);
        recyclerView.setAdapter(movieAdapterFavorite);
        movieAdapterFavorite.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);

        movieAdapterFavorite.setOnItemClickListener(new MovieAdapterFavorite.ClickListener() {
          @Override
          public void onItemClick(int position, View v) {

            Intent intent = new Intent(getActivity(), DetailMovieActivity.class);

            String movie_id = String.valueOf(getItem(position).getMovie_id());
            intent.putExtra(EXTRAS_MOVIE_ID, movie_id);
            intent.putExtra(EXTRAS_DELETE, true);
            startActivity(intent);

          }
        });


        hideLoading();
      }

      @Override
      protected void onPreExecute() {
        super.onPreExecute();
        showLoading();
      }
    };

    task.execute();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  public void hideLoading() {
    loading.setVisibility(View.GONE);
  }

  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
  }

  private Movie getItem(int position){
    if (!list.moveToPosition(position)) {
      throw new IllegalStateException("Position invalid");
    }
    return new Movie(list);
  }
}
