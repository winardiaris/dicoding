package xyz.winardiaris.android.moviedbwidget;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import xyz.winardiaris.android.moviedbwidget.model.Movie;
import xyz.winardiaris.android.moviedbwidget.utils.Utils;

import static xyz.winardiaris.android.moviedbwidget.BuildConfig.API_KEY;
import static xyz.winardiaris.android.moviedbwidget.BuildConfig.BASE_URL;

public class DetailMovieActivity extends AppCompatActivity
    implements View.OnClickListener {
  private static final String TAG             = "DetailMovieActivity";
  public static final String EXTRAS_MOVIE_ID = "EXTRAS_MOVIE_ID";
  public static final String EXTRAS_DELETE   = "EXTRAS_DELETE";

  String movie_id       = "";
  String jsonString     = "";

  Context               context;
  ImageView             movie_poster;
  ProgressBar           spinner;
  RelativeLayout        detail_movie;
  RelativeLayout        loading;
  TextView              movie_title;
  TextView              movie_tagline;
  TextView              movie_overview;
  TextView              movie_release_date;
  TextView              movie_vote;
  TextView              movie_lang;
  TextView              movie_production_companies;
  Movie movie;
  Utils utils;
  Button                btnFavorite;
  Button                btnDelete;
  Intent                intent;
  boolean               delete;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_movie);
    this.intent   = getIntent();
    this.utils    = new Utils();
    this.context  = this.getBaseContext();

    this.detail_movie               = (RelativeLayout) findViewById(R.id.detail_movie);
    this.movie_id                   = intent.getStringExtra(EXTRAS_MOVIE_ID);
    this.movie_lang                 = (TextView) findViewById(R.id.movie_lang);
    this.movie_overview             = (TextView) findViewById(R.id.movie_overview);
    this.movie_release_date         = (TextView) findViewById(R.id.movie_release_date);
    this.movie_tagline              = (TextView) findViewById(R.id.movie_tagline);
    this.movie_title                = (TextView) findViewById(R.id.movie_title);
    this.movie_poster               = (ImageView) findViewById(R.id.movie_poster);
    this.movie_production_companies = (TextView) findViewById(R.id.movie_production_companies);
    this.movie_vote                 = (TextView) findViewById(R.id.movie_vote);
    this.loading                    = (RelativeLayout) findViewById(R.id.loading);
    this.spinner                    = (ProgressBar)findViewById(R.id.progressBar);
    this.btnFavorite                = (Button) findViewById(R.id.btn_set_favorite);
    this.btnDelete                  = (Button) findViewById(R.id.btn_delete);
    this.delete                     = intent.getBooleanExtra(EXTRAS_DELETE, false);

    btnFavorite.setOnClickListener(this);
    btnDelete.setOnClickListener(this);
    getDetailMovie();
    showDeleteButton();
  }

  private void showDeleteButton() {
    if(delete) {
      btnFavorite.setVisibility(View.GONE);
      btnDelete.setVisibility(View.VISIBLE);
    }
  }

  public void getDetailMovie() {
    AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void> () {
      @Override
      protected void onPreExecute() {
        super.onPreExecute();
        showLoading();
      }

      @Override
      protected Void doInBackground(Integer... integers) {
        String URL = BASE_URL + "movie/" + movie_id +"?api_key=" + API_KEY;
        Log.d(TAG, "doInBackground: start: " + URL);
        jsonString = utils.getJsonString(URL);
        return null;
      }

      @Override
      protected void onPostExecute(Void aVoid) {
        movie = new Movie(jsonString);
        initViewData();
        hideLoading();
      }
    };
    task.execute();
  }

  private void showLoading() {
    detail_movie.setVisibility(View.GONE);
    loading.setVisibility(View.VISIBLE);
  }

  private void hideLoading() {
    detail_movie.setVisibility(View.VISIBLE);
    loading.setVisibility(View.GONE);
  }

  private void initViewData() {
    Utils utils = new Utils();
    movie_lang.setText(movie.getOriginal_language().toUpperCase());
    movie_overview.setText(movie.getOverview());
    movie_production_companies.setText(movie.getProduction_companies());
    movie_release_date.setText(utils.humanReadableDate(movie.getRelease_date()));
    movie_tagline.setText(movie.getTagline());
    movie_title.setText(movie.getTitle());
    movie_vote.setText(movie.getVote_average());

    Glide.with(getBaseContext())
        .load(movie.getPoster())
        .override(500, 700)
        .into(movie_poster);

    getSupportActionBar().setTitle(movie.getTitle());
  }


  @Override
  public void onClick(View v) {
    if (v.getId() == R.id.btn_set_favorite) {
      Toast.makeText(context, context.getResources().getString(R.string.favorite)+ " " +
          movie.getTitle(), Toast.LENGTH_SHORT).show();
      utils.setFavorite(context, movie);
    } else if (v.getId() == R.id.btn_delete) {
      Toast.makeText(context, context.getResources().getString(R.string.delete)+ " " +
          movie.getTitle(), Toast.LENGTH_SHORT).show();
      utils.setDelete(context, movie);
    }
  }
}
