package xyz.winardiaris.android.moviedbwidget.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import xyz.winardiaris.android.moviedbwidget.BuildConfig;
import xyz.winardiaris.android.moviedbwidget.model.Movie;
import xyz.winardiaris.android.moviedbwidget.R;
import xyz.winardiaris.android.moviedbwidget.databases.FavoriteHelper;
import xyz.winardiaris.android.moviedbwidget.utils.CustomOnItemClickListener;
import xyz.winardiaris.android.moviedbwidget.utils.Utils;

/**
 * Created by winardiaris on 10/11/17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.CardViewViewHolder> {
  private ArrayList<Movie> listMovie;
  private Context context;
  private Utils utils;
  private static  String LIMIT_OVERVIEW = BuildConfig.LIMIT_OVERVIEW;
  private static ClickListener clickListener;
  private FavoriteHelper favoriteHelper;

  public MovieAdapter(Context context) {
    this.context = context;
  }

  private ArrayList<Movie> getListMovie() {
    return listMovie;
  }

  public void setListMovie(ArrayList<Movie> listMovie) {
    this.listMovie = listMovie;
  }

  @Override
  public CardViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_movie, parent, false);
    return new CardViewViewHolder(view);
  }

  @Override
  public void onBindViewHolder(CardViewViewHolder holder, int position) {
    Movie m = getListMovie().get(position);
    utils = new Utils();

    Glide.with(context)
        .load(m.getPoster())
        .override(300,400)
        .into(holder.moviePoster);

    holder.movieTitle.setText(m.getTitle());
    holder.movieOverview.setText(utils.getSafeSubstring(m.getOverview(), Integer.valueOf(MovieAdapter.LIMIT_OVERVIEW)));
    holder.movieReleaseDate.setText(utils.humanReadableDate(m.getRelease_date()));

    holder.buttonFavorite.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
      @Override
      public void onItemClicked(View view, int position) {
        Toast.makeText(context, context.getResources().getString(R.string.favorite) + " " + getListMovie().get(position).getTitle(), Toast.LENGTH_SHORT).show();
        utils.setFavorite(context, getListMovie().get(position));
      }
    }));

    holder.buttonShare.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
      @Override
      public void onItemClicked(View view, int position) {
        Toast.makeText(context, context.getResources().getString(R.string.share) + " " + getListMovie().get(position).getTitle(), Toast.LENGTH_SHORT).show();
      }
    }));


  }

  @Override
  public int getItemCount() {
    return listMovie.size();
  }

  public class CardViewViewHolder extends RecyclerView.ViewHolder
  implements View.OnClickListener {
    ImageView moviePoster;
    TextView movieTitle;
    TextView movieOverview;
    TextView movieReleaseDate;
    Button buttonFavorite;
    Button buttonShare;

    public CardViewViewHolder(View itemView) {
      super(itemView);

      moviePoster       = itemView.findViewById(R.id.movie_poster);
      movieTitle        = itemView.findViewById(R.id.movie_title);
      movieOverview     = itemView.findViewById(R.id.movie_overview);
      movieReleaseDate  = itemView.findViewById(R.id.movie_release_date);
      buttonFavorite    = itemView.findViewById(R.id.btn_set_favorite);
      buttonShare       = itemView.findViewById(R.id.btn_set_share);

      itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
      clickListener.onItemClick(getAdapterPosition(), v);
    }
  }


  public void setOnItemClickListener(ClickListener clickListener) {
    MovieAdapter.clickListener = clickListener;
  }

  public interface ClickListener {
    void onItemClick(int position, View v);
  }
}
