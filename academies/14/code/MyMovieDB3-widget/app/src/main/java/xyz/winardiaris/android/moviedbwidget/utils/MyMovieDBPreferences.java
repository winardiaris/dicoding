package xyz.winardiaris.android.moviedbwidget.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by winardiaris on 03/12/17.
 */

public class MyMovieDBPreferences {
  private final String PREF_NAME = "MyMovieDBPreferences";
  private final String LAST_VISITED         = "lastVisited";
  private final String NEXT_REMIDER         = "nextReminder";
  private final String UPCOMING_TIME_1      = "upcomingTime1";
  private final String UPCOMING_DATE_1      = "upcomingDate1";
  private final String UPCOMING_TITLE_1     = "upcomingTitle1";
  private final String UPCOMING_MOVIE_ID_1  = "upcomingMovieId1";
  private final String UPCOMING_TIME_2      = "upcomingTime2";
  private final String UPCOMING_DATE_2      = "upcomingDate2";
  private final String UPCOMING_TITLE_2     = "upcomingTitle2";
  private final String UPCOMING_MOVIE_ID_2  = "upcomingMovieId2";


  private SharedPreferences         sharedPreferences;
  private SharedPreferences.Editor  editor;

  public MyMovieDBPreferences(Context context) {
    sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    editor = sharedPreferences.edit();
  }

  public void setLastVisited(String dateTime){
    editor.putString(LAST_VISITED, dateTime);
    editor.commit();
  }

  public String getLastVisited() {
    return sharedPreferences.getString(LAST_VISITED, null);
  }

  public void setNextReminder(Integer second) {
    editor.putInt(NEXT_REMIDER, second);
    editor.commit();
  }

  public int getNextReminder() {
    return sharedPreferences.getInt(NEXT_REMIDER, 3600);
  }

  public void setUpcoming1(String movieId, Integer second, String date, String title) {
    editor.putString(UPCOMING_MOVIE_ID_1, movieId);
    editor.putInt(UPCOMING_TIME_1, second);
    editor.putString(UPCOMING_DATE_1, date);
    editor.putString(UPCOMING_TITLE_1, title);
    editor.commit();
  }

  public int getUpcomingTime1() {
    return sharedPreferences.getInt(UPCOMING_TIME_1, 0);
  }

  public String getUpcomingTitle1() {
    return sharedPreferences.getString(UPCOMING_TITLE_1, null);
  }

  public String getUpcomingDate1() {
    return sharedPreferences.getString(UPCOMING_DATE_1, null);
  }

  public String getUpcomingMovieId1() {
    return sharedPreferences.getString(UPCOMING_MOVIE_ID_1, null);
  }

  public void setUpcoming2(String movieId, Integer second, String date, String title) {
    editor.putString(UPCOMING_MOVIE_ID_2, movieId);
    editor.putInt(UPCOMING_TIME_2, second);
    editor.putString(UPCOMING_DATE_2, date);
    editor.putString(UPCOMING_TITLE_2, title);
    editor.commit();
  }

  public int getUpcomingTime2() {
    return sharedPreferences.getInt(UPCOMING_TIME_2, 0);
  }

  public String getUpcomingTitle2() {
    return sharedPreferences.getString(UPCOMING_TITLE_2, null);
  }

  public String getUpcomingDate2() {
    return sharedPreferences.getString(UPCOMING_DATE_2, null);
  }

  public String getUpcomingMovieId2() {
    return sharedPreferences.getString(UPCOMING_MOVIE_ID_2, null);
  }

  public void clear() {
    editor.clear();
    editor.commit();
  }
}
