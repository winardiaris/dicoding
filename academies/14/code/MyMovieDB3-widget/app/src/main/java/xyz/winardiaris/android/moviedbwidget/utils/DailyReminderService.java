package xyz.winardiaris.android.moviedbwidget.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import xyz.winardiaris.android.moviedbwidget.MainActivity;
import xyz.winardiaris.android.moviedbwidget.R;

/**
 * Created by winardiaris on 03/12/17.
 */

public class DailyReminderService extends GcmTaskService {
  public static final String TAG = "DailyReminder";
  public static String TAG_DAILY_REMINDER_LOG = "DailyReminderTask";

  public static final int NOTIFICAITION_ID    = 1;

  private Handler handler = new Handler();
  private NotificationCompat.Builder notification;
  @Override
  public int onRunTask(TaskParams taskParams) {
    int result = 0;

    if (taskParams.getTag().equals(TAG_DAILY_REMINDER_LOG)){
      sendNotificationDailyReminder();
      result = GcmNetworkManager.RESULT_SUCCESS;
    }
    return result;
  }

  private Runnable runnable = new Runnable() {
    @Override
    public void run() {
      NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
      nm.notify(NOTIFICAITION_ID, notification.build());
    }
  };

  public void sendNotificationDailyReminder() {
    Intent intent = new Intent(this, MainActivity.class);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

    notification = (NotificationCompat.Builder) new NotificationCompat
        .Builder(this)
        .setSmallIcon(R.mipmap.mymoviedb_round)
        .setContentIntent(pendingIntent)
        .setLargeIcon(BitmapFactory
            .decodeResource(getResources()
                , R.mipmap.mymoviedb))
        .setContentTitle(getResources()
            .getString(R.string.app_name))
        .setContentText(getResources()
            .getString(R.string.notification_miss_you))
        .setSubText(getResources()
            .getString(R.string.notification_tap_here))
        .setAutoCancel(true);

    handler.postDelayed(runnable, 0);
  }

  @Override
  public void onInitializeTasks() {
    super.onInitializeTasks();
    MyMovieDbTask mSchedulerTask = new MyMovieDbTask(this);
    MyMovieDBPreferences preferences = new MyMovieDBPreferences(this);
    mSchedulerTask.dailyReminderTask(preferences.getNextReminder());
  }
}
