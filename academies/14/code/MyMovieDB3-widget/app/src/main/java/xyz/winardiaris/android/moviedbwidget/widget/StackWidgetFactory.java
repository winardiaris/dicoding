package xyz.winardiaris.android.moviedbwidget.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import xyz.winardiaris.android.moviedbwidget.R;
import xyz.winardiaris.android.moviedbwidget.databases.FavoriteHelper;
import xyz.winardiaris.android.moviedbwidget.model.Movie;

import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.CONTENT_URI;

/**
 * Created by winardiaris on 03/12/17.
 */

public class StackWidgetFactory implements RemoteViewsService.RemoteViewsFactory {
  private static final String TAG = StackWidgetFactory.class.getSimpleName();
  private Context context;
  private int mAppWidgetId;
  private ArrayList<Movie> listFavorite = new ArrayList<>();
  private FavoriteHelper helper;

  public StackWidgetFactory(Context context, Intent intent) {
    this.context = context;
    mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
        AppWidgetManager.INVALID_APPWIDGET_ID);
  }

  @Override
  public void onCreate() {
    helper = new FavoriteHelper(context);
    helper.open();
  }

  @Override
  public void onDataSetChanged() {
    listFavorite = helper.query();
  }

  @Override
  public void onDestroy() {

  }

  @Override
  public int getCount() {
    return listFavorite.size();
  }

  @Override
  public RemoteViews getViewAt(int position) {
    RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_item);
    Movie movie = listFavorite.get(position);

    Bitmap bmp = null;
    try {

      bmp = Glide.with(context)
          .load(movie.getPoster())
          .asBitmap()
          .error(new ColorDrawable(context.getResources().getColor(R.color.colorAccent)))
          .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();

    } catch (InterruptedException | ExecutionException e) {
      Log.d("Widget Load Error", "error");
    }
    rv.setImageViewBitmap(R.id.imageView, bmp);

    Bundle extras = new Bundle();
    extras.putInt(FavoriteMovieWidget.EXTRA_ITEM, position);
    extras.putInt(FavoriteMovieWidget.MOVIE_ID, movie.getMovie_id());
    Intent fillInIntent = new Intent();
    fillInIntent.putExtras(extras);

    rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
    return rv;
  }

  @Override
  public RemoteViews getLoadingView() {
    return null;
  }

  @Override
  public int getViewTypeCount() {
    return 1;
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  @Override
  public boolean hasStableIds() {
    return false;
  }

}
