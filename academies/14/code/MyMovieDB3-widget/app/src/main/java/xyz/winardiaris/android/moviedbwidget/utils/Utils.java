package xyz.winardiaris.android.moviedbwidget.utils;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import xyz.winardiaris.android.moviedbwidget.R;
import xyz.winardiaris.android.moviedbwidget.databases.FavoriteHelper;
import xyz.winardiaris.android.moviedbwidget.model.Movie;
import xyz.winardiaris.android.moviedbwidget.widget.FavoriteMovieWidget;

import static xyz.winardiaris.android.moviedbwidget.BuildConfig.API_KEY;
import static xyz.winardiaris.android.moviedbwidget.BuildConfig.BASE_URL;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.CONTENT_URI;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.MOVIE_ID;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.ORIGINAL_LANGUAGE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.ORIGINAL_TITLE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.OVERVIEW;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.POSTER;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.PRODUCTION_COMPANIES;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.RELEASE_DATE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.TAGLINE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.TITLE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.VOTE_AVERAGE;

/**
 * Created by winardiaris on 23/10/17.
 */

public class Utils {
  private static final String TAG = "Utils";
  public static final DateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd");
  public Date currentDate = new Date();

  String                jsonString;
  FavoriteHelper        favoriteHelper;
  MyMovieDBPreferences  preferences;
  Movie                 movie;
  ArrayList<Movie>      arrayList;
  int                   nextReminderInSecond;


  public void initPreferences(Context context) {
    preferences = new MyMovieDBPreferences(context);

    nextReminderInSecond     = 86400; // 24 * 60 * 60
    Calendar nextReminder    = Calendar.getInstance();
    nextReminder.setTime(currentDate);
    nextReminder.add(Calendar.SECOND, nextReminderInSecond);

    preferences.setLastVisited(dateFormat.format(currentDate));
    preferences.setNextReminder(nextReminderInSecond);

    initUpcomingMoviesPreferences();

  }

  public void initUpcomingMoviesPreferences() {
    AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
      @Override
      protected Void doInBackground(Integer... integers) {
        Log.d(TAG, "doInBackground: start");
        SyncHttpClient client = new SyncHttpClient();
        String URL            = BASE_URL + "movie/upcoming?api_key=" + API_KEY;
        arrayList             = new ArrayList<>();

        client.get(URL, new AsyncHttpResponseHandler() {
          @Override
          public void onStart() {
            super.onStart();
            setUseSynchronousMode(true);
          }

          @Override
          public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            String result = new String(responseBody);
            JSONObject responseObject = null;
            try {
              responseObject    = new JSONObject(result);
              JSONArray list    = responseObject.getJSONArray("results");

              for (int i = 0 ; i < list.length() ; i++) {
                JSONObject movie  = list.getJSONObject(i);
                Movie item        = new Movie(movie);
                Date releaseDate  = dateFormat.parse(item.getRelease_date());
                if(currentDate.before(releaseDate)) {
                  arrayList.add(item);
                }
              }
            } catch (JSONException e) {
              e.printStackTrace();
            } catch (ParseException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

          }
        });
        return null;
      }

      @Override
      protected void onPostExecute(Void aVoid) {
        sortMovieList(arrayList);
        try {
          int upcomingTime1 = diffDaysInSecond(currentDate, dateFormat.parse(arrayList.get(0).getRelease_date()));
          int upcomingTime2 = diffDaysInSecond(currentDate, dateFormat.parse(arrayList.get(1).getRelease_date()));

          String upcomingMovieId1 = String.valueOf(arrayList.get(0).getMovie_id());
          String upcomingMovieId2 = String.valueOf(arrayList.get(1).getMovie_id());

          String upcomingDate1 = arrayList.get(0).getRelease_date();
          String upcomingDate2 = arrayList.get(1).getRelease_date();

          String upcomingTitle1 = arrayList.get(0).getTitle();
          String upcomingTitle2 = arrayList.get(1).getTitle();

          preferences.setUpcoming1(upcomingMovieId1, upcomingTime1, upcomingDate1, upcomingTitle1);
          preferences.setUpcoming2(upcomingMovieId2, upcomingTime2, upcomingDate2, upcomingTitle2);
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }

      @Override
      protected void onPreExecute() {
        super.onPreExecute();
      }
    };
    task.execute();
  }

  public int diffDaysInSecond(Date currentDate, Date releaseDate){
    int result = (int) ((releaseDate.getTime()-currentDate.getTime())/1000);
    return result;
  }

  public String humanReadableDate(String dateInString) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    try {
      Date date = formatter.parse(dateInString);
      formatter.applyPattern("E, dd MMM yyyy");
      return formatter.format(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public String getSafeSubstring(String s, int maxLength){
    if(!TextUtils.isEmpty(s)){
      if(s.length() >= maxLength){
        return s.substring(0, maxLength) + "...";
      }
    }
    return s;
  }

  public String getJsonString(String url) {
    SyncHttpClient client = new SyncHttpClient();
    client.get(url, new AsyncHttpResponseHandler() {
      @Override
      public void onStart() {
        super.onStart();
        setUseSynchronousMode(true);
      }

      @Override
      public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
          jsonString =  new String(responseBody);
        } catch (Exception e) {
          e.printStackTrace();
          Log.e("getJsonString", "onSuccess: request fail", e);
        }
      }

      @Override
      public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

      }
    });
    return jsonString;
  }

  public void setFavorite(Context context, Movie movie) {
    Log.d("UTILS", "setFavorite: " + movie.getTitle());
    ContentValues contentValues = convertToContentValues(movie);
    context.getContentResolver().insert(CONTENT_URI, contentValues);
    updateWidget(context);
  }

  public void setDelete(Context context, Movie movie) {
    Log.d("UTILS", "delete: " + movie.getTitle());
    favoriteHelper = new FavoriteHelper(context);
    favoriteHelper.open();
    favoriteHelper.delete(movie.getId());
    favoriteHelper.close();
    updateWidget(context);
  }

  public void updateWidget(Context context) {
    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
    ComponentName thisWidget = new ComponentName(context, FavoriteMovieWidget.class);
    int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
    appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view);
  }

  public ContentValues convertToContentValues(Movie movie) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(MOVIE_ID, movie.getMovie_id());
    contentValues.put(TITLE, movie.getTitle());
    contentValues.put(OVERVIEW, movie.getOverview());
    contentValues.put(POSTER, movie.getPoster());
    contentValues.put(RELEASE_DATE, movie.getRelease_date());
    contentValues.put(TAGLINE, movie.getTagline());
    contentValues.put(ORIGINAL_LANGUAGE, movie.getOriginal_language());
    contentValues.put(ORIGINAL_TITLE, movie.getOriginal_title());
    contentValues.put(VOTE_AVERAGE, movie.getVote_average());
    contentValues.put(PRODUCTION_COMPANIES, movie.getProduction_companies());

    return contentValues;
  }

  public void sortMovieList(ArrayList arrayList) {
    Collections.sort(arrayList, new Comparator<Movie>() {
      @Override
      public int compare(Movie o1, Movie o2) {
        return o1.getRelease_date().compareTo(o2.getRelease_date());
      }
    });
  }
}
