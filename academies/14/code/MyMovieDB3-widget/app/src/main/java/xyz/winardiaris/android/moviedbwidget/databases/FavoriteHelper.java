package xyz.winardiaris.android.moviedbwidget.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import xyz.winardiaris.android.moviedbwidget.model.Movie;

import static android.provider.BaseColumns._ID;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.MOVIE_ID;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.ORIGINAL_LANGUAGE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.ORIGINAL_TITLE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.OVERVIEW;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.POSTER;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.PRODUCTION_COMPANIES;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.RELEASE_DATE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.TAGLINE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.TITLE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.MovieColumns.VOTE_AVERAGE;
import static xyz.winardiaris.android.moviedbwidget.databases.FavoriteContract.TABLE_NAME;

/**
 * Created by winardiaris on 21/11/17.
 */

public class FavoriteHelper {
  private static String DATABASE_TABLE = TABLE_NAME;
  private Context context;
  private DatabaseHelper databaseHelper;
  private SQLiteDatabase database;

  public FavoriteHelper(Context context) {
    this.context = context;
  }

  public FavoriteHelper open() throws SQLException {
    databaseHelper = new DatabaseHelper(context);
    database = databaseHelper.getWritableDatabase();
    return this;
  }

  public void close() {
    databaseHelper.close();
  }

  public ArrayList<Movie> query() {
    ArrayList<Movie> arrayList = new ArrayList<>();
    Cursor cursor = database.query(DATABASE_TABLE, null, null, null,
                              null,null, _ID + " DESC", null);
    cursor.moveToFirst();
    Movie movie;
    if(cursor.getCount()>0) {
      do {
        movie = new Movie();
        movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        movie.setMovie_id(cursor.getInt(cursor.getColumnIndexOrThrow(MOVIE_ID)));
        movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
        movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
        movie.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));
        movie.setRelease_date(cursor.getString(cursor.getColumnIndexOrThrow(RELEASE_DATE)));
        movie.setTagline(cursor.getString(cursor.getColumnIndexOrThrow(TAGLINE)));
        movie.setOriginal_language(cursor.getString(cursor.getColumnIndexOrThrow(ORIGINAL_LANGUAGE)));
        movie.setOriginal_title(cursor.getString(cursor.getColumnIndexOrThrow(ORIGINAL_TITLE)));
        movie.setVote_average(cursor.getString(cursor.getColumnIndexOrThrow(VOTE_AVERAGE)));
        movie.setProduction_companies(cursor.getString(cursor.getColumnIndexOrThrow(PRODUCTION_COMPANIES)));

        arrayList.add(movie);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }

  public long insert(Movie movie) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(MOVIE_ID, movie.getMovie_id());
    contentValues.put(TITLE, movie.getTitle());
    contentValues.put(OVERVIEW, movie.getOverview());
    contentValues.put(POSTER, movie.getPoster());
    contentValues.put(RELEASE_DATE, movie.getRelease_date());
    contentValues.put(TAGLINE, movie.getTagline());
    contentValues.put(ORIGINAL_LANGUAGE, movie.getOriginal_language());
    contentValues.put(ORIGINAL_TITLE, movie.getOriginal_title());
    contentValues.put(VOTE_AVERAGE, movie.getVote_average());
    contentValues.put(PRODUCTION_COMPANIES, movie.getProduction_companies());
    return database.insert(DATABASE_TABLE, null, contentValues);
  }

  public long update(Movie movie) {
    ContentValues args = new ContentValues();
    args.put(MOVIE_ID, movie.getMovie_id());
    args.put(TITLE, movie.getTitle());
    args.put(OVERVIEW, movie.getOverview());
    args.put(POSTER, movie.getPoster());
    args.put(RELEASE_DATE, movie.getRelease_date());
    args.put(TAGLINE, movie.getTagline());
    args.put(ORIGINAL_LANGUAGE, movie.getOriginal_language());
    args.put(ORIGINAL_TITLE, movie.getOriginal_title());
    args.put(VOTE_AVERAGE, movie.getVote_average());
    args.put(PRODUCTION_COMPANIES, movie.getProduction_companies());
    return database.update(DATABASE_TABLE, args,_ID + "='" + movie.getId() + "'", null);
  }

  public int delete(int id){
    return database.delete(DATABASE_TABLE, _ID + " = '" + id + "'", null);
  }


  public Cursor queryByIdProvider(String id){
    return database.query(DATABASE_TABLE,null
        ,_ID + " = ?"
        ,new String[]{id}
        ,null
        ,null
        ,null
        ,null);
  }
  public Cursor queryProvider(){
    return database.query(DATABASE_TABLE
        ,null
        ,null
        ,null
        ,null
        ,null
        ,_ID + " DESC");
  }
  public long insertProvider(ContentValues values){
    return database.insert(DATABASE_TABLE,null,values);
  }
  public int updateProvider(String id,ContentValues values){
    return database.update(DATABASE_TABLE,values,_ID +" = ?",new String[]{id} );
  }
  public int deleteProvider(String id){
    return database.delete(DATABASE_TABLE,_ID + " = ?", new String[]{id});
  }

}
