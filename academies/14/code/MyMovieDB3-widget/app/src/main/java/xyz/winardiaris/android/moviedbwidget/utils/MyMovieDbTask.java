package xyz.winardiaris.android.moviedbwidget.utils;

import android.content.Context;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

/**
 * Created by winardiaris on 03/12/17.
 */

public class MyMovieDbTask {
  private GcmNetworkManager gcmNetworkManager;

  public MyMovieDbTask(Context context){
    gcmNetworkManager = GcmNetworkManager.getInstance(context);
  }

  public void dailyReminderTask(int second) {
    Task periodicTask = new PeriodicTask.Builder()
        .setService(DailyReminderService.class)
        .setPeriod(second)
        .setFlex(10)
        .setTag(DailyReminderService.TAG_DAILY_REMINDER_LOG)
        .setPersisted(true)
        .build();
    gcmNetworkManager.schedule(periodicTask);
  }

  public void cancelDailyReminderTask(){
    if (gcmNetworkManager != null){
      gcmNetworkManager.cancelTask(DailyReminderService.TAG_DAILY_REMINDER_LOG, DailyReminderService.class);
    }
  }

  public void upcoming1Task(int second) {
    Task periodicTask = new PeriodicTask.Builder()
        .setService(Upcoming1Service.class)
        .setPeriod(second)
        .setFlex(10)
        .setTag(Upcoming1Service.TAG_LOG)
        .setPersisted(true)
        .build();
    gcmNetworkManager.schedule(periodicTask);
  }

  public void cancelUpcoming1Task(){
    if (gcmNetworkManager != null){
      gcmNetworkManager.cancelTask(Upcoming1Service.TAG_LOG, Upcoming1Service.class);
    }
  }

  public void upcoming2Task(int second) {
    Task periodicTask = new PeriodicTask.Builder()
        .setService(Upcoming2Service.class)
        .setPeriod(second)
        .setFlex(10)
        .setTag(Upcoming2Service.TAG_LOG)
        .setPersisted(true)
        .build();
    gcmNetworkManager.schedule(periodicTask);
  }

  public void cancelUpcoming2Task(){
    if (gcmNetworkManager != null){
      gcmNetworkManager.cancelTask(Upcoming2Service.TAG_LOG, Upcoming2Service.class);
    }
  }
}
