package xyz.winardiaris.android.moviedbwidget.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import xyz.winardiaris.android.moviedbwidget.DetailMovieActivity;
import xyz.winardiaris.android.moviedbwidget.R;

import static xyz.winardiaris.android.moviedbwidget.DetailMovieActivity.EXTRAS_DELETE;
import static xyz.winardiaris.android.moviedbwidget.DetailMovieActivity.EXTRAS_MOVIE_ID;

/**
 * Implementation of App Widget functionality.
 */
public class FavoriteMovieWidget extends AppWidgetProvider {
  public static final String DETAIL_ACTION  = "xyz.winardiaris.android.DETAIL_ACTION";
  public static final String EXTRA_ITEM     = "xyz.winardiaris.android.EXTRA_ITEM";
  public static final String MOVIE_ID       = "xyz.winardiaris.android.MOVIE_ID";

  public void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                              int appWidgetId) {

    Intent intent = new Intent(context, StackWidgetService.class);

    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
    intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

    RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.favorite_movie_widget);
    views.setRemoteAdapter( R.id.stack_view, intent);
    views.setEmptyView(R.id.stack_view, R.id.empty_view);

    Intent newIntent = new Intent(context, FavoriteMovieWidget.class);

    newIntent.setAction(FavoriteMovieWidget.DETAIL_ACTION);
    newIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

    intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, newIntent,
        PendingIntent.FLAG_UPDATE_CURRENT);
    views.setPendingIntentTemplate(R.id.stack_view, pendingIntent);

    appWidgetManager.updateAppWidget(appWidgetId, views);
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    AppWidgetManager mgr = AppWidgetManager.getInstance(context);
    if (intent.getAction().equals(DETAIL_ACTION)) {
      int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
          AppWidgetManager.INVALID_APPWIDGET_ID);
      int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0);
      int movie_id = intent.getIntExtra(MOVIE_ID, 0);

      Intent detailIntent = new Intent (context, DetailMovieActivity.class);
      detailIntent.setFlags (Intent.FLAG_ACTIVITY_NEW_TASK);
      detailIntent.putExtra(EXTRAS_MOVIE_ID, String.valueOf(movie_id));
      detailIntent.putExtra(EXTRAS_DELETE, true);
      context.startActivity (detailIntent);
    }
    super.onReceive(context, intent);
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    // There may be multiple widgets active, so update all of them
    for (int appWidgetId : appWidgetIds) {
      updateAppWidget(context, appWidgetManager, appWidgetId);
    }
  }

  @Override
  public void onEnabled(Context context) {
    // Enter relevant functionality for when the first widget is created
  }

  @Override
  public void onDisabled(Context context) {
    // Enter relevant functionality for when the last widget is disabled
  }
}

