package xyz.winardiaris.android.moviedbwidget.upcoming;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import xyz.winardiaris.android.moviedbwidget.DetailMovieActivity;
import xyz.winardiaris.android.moviedbwidget.R;
import xyz.winardiaris.android.moviedbwidget.adapter.MovieAdapter;
import xyz.winardiaris.android.moviedbwidget.model.Movie;
import xyz.winardiaris.android.moviedbwidget.utils.Utils;

import static xyz.winardiaris.android.moviedbwidget.BuildConfig.API_KEY;
import static xyz.winardiaris.android.moviedbwidget.BuildConfig.BASE_URL;
import static xyz.winardiaris.android.moviedbwidget.DetailMovieActivity.EXTRAS_MOVIE_ID;

public class UpcomingFragment extends Fragment {
  private static final String TAG     = UpcomingFragment.class.getSimpleName();
  protected View            view;
  protected RecyclerView    recyclerView;
  private ArrayList<Movie>  arrayList;
  private RelativeLayout    loading;
  Utils utils = new Utils();

  public UpcomingFragment() {
    // Required empty public constructor
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    view = inflater.inflate(R.layout.fragment_upcoming, container, false);
    view.setTag(TAG);
    this.loading = view.findViewById(R.id.loading);

    arrayList = new ArrayList<>();
    loadData();
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  private void loadData() {
    AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
      @Override
      protected Void doInBackground(Integer... integers) {
        Log.d(TAG, "doInBackground: start");
        SyncHttpClient client = new SyncHttpClient();
        String URL            = BASE_URL + "movie/upcoming?api_key=" + API_KEY;
        arrayList             = new ArrayList<>();

        client.get(URL, new AsyncHttpResponseHandler() {
          @Override
          public void onStart() {
            super.onStart();
            setUseSynchronousMode(true);
          }

          @Override
          public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            String result = new String(responseBody);
            JSONObject responseObject = null;
            try {
              responseObject    = new JSONObject(result);
              JSONArray list    = responseObject.getJSONArray("results");

              for (int i = 0 ; i < list.length() ; i++) {
                JSONObject movie  = list.getJSONObject(i);
                Movie item        = new Movie(movie);
                Date releaseDate  = utils.dateFormat.parse(item.getRelease_date());
                if(utils.currentDate.before(releaseDate)) {
                  arrayList.add(item);
                }
              }
            } catch (JSONException e) {
              e.printStackTrace();
            } catch (ParseException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

          }
        });
        return null;
      }

      @Override
      protected void onPostExecute(Void aVoid) {
        Utils utils = new Utils();
        recyclerView = view.findViewById(R.id.rv_upcoming);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        MovieAdapter movieAdapter = new MovieAdapter(getActivity());
        utils.sortMovieList(arrayList);
        movieAdapter.setListMovie(arrayList);
        recyclerView.setAdapter(movieAdapter);

        movieAdapter.setOnItemClickListener(new MovieAdapter.ClickListener() {
          @Override
          public void onItemClick(int position, View v) {
            Log.d(TAG, "onItemClick: " + arrayList.get(position).getId());

            Intent intent = new Intent(getActivity(), DetailMovieActivity.class);

            String movie_id = String.valueOf(arrayList.get(position).getId());
            intent.putExtra(EXTRAS_MOVIE_ID, movie_id);
            startActivity(intent);

          }
        });

        hideLoading();
      }

      @Override
      protected void onPreExecute() {
        super.onPreExecute();
        showLoading();

      }
    };

    task.execute();
  }
  public void hideLoading() {
    loading.setVisibility(View.GONE);
  }

  public void showLoading() {
    loading.setVisibility(View.VISIBLE);
  }
}
