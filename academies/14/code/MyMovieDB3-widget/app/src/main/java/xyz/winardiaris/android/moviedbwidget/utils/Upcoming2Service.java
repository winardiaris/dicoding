package xyz.winardiaris.android.moviedbwidget.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import xyz.winardiaris.android.moviedbwidget.DetailMovieActivity;
import xyz.winardiaris.android.moviedbwidget.R;

import static xyz.winardiaris.android.moviedbwidget.DetailMovieActivity.EXTRAS_MOVIE_ID;

/**
 * Created by winardiaris on 03/12/17.
 */

public class Upcoming2Service extends GcmTaskService {
  public static final String TAG  = "Upcoming2";
  public static String TAG_LOG    = "Upcoming2Task";

  public static final int NOTIFICAITION_ID    = 3;

  private Handler handler = new Handler();
  private NotificationCompat.Builder notification;
  private MyMovieDBPreferences preferences;
  @Override
  public int onRunTask(TaskParams taskParams) {
    int result = 0;

    if (taskParams.getTag().equals(TAG_LOG)){
      sendNotificationUpcoming2();
      result = GcmNetworkManager.RESULT_SUCCESS;
    }
    return result;
  }

  private Runnable runnable = new Runnable() {
    @Override
    public void run() {
      NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
      nm.notify(NOTIFICAITION_ID, notification.build());
    }
  };

  public void sendNotificationUpcoming2() {
    Intent intent = new Intent(this, DetailMovieActivity.class);
    intent.putExtra(EXTRAS_MOVIE_ID, preferences.getUpcomingMovieId2());
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

    notification = (NotificationCompat.Builder) new NotificationCompat
        .Builder(this)
        .setSmallIcon(R.mipmap.mymoviedb_round)
        .setContentIntent(pendingIntent)
        .setLargeIcon(BitmapFactory
            .decodeResource(getResources()
                , R.mipmap.mymoviedb))
        .setContentTitle(preferences.getUpcomingTitle2())
        .setContentText(
            getResources().getString(R.string.notification_this_day) + " " +
                preferences.getUpcomingTitle2() + " " +
            getResources().getString(R.string.notification_release)
        )
        .setSubText(getResources()
            .getString(R.string.notification_tap_here))
        .setAutoCancel(true);

    handler.postDelayed(runnable, 0);
  }

  @Override
  public void onInitializeTasks() {
    super.onInitializeTasks();
    MyMovieDbTask mSchedulerTask = new MyMovieDbTask(this);
    preferences = new MyMovieDBPreferences(this);
    mSchedulerTask.upcoming2Task(preferences.getUpcomingTime2());
  }
}
