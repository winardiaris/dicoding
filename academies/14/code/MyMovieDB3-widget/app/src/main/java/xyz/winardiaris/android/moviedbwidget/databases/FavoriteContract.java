package xyz.winardiaris.android.moviedbwidget.databases;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by winardiaris on 21/11/17.
 */

public class FavoriteContract {
  public static String TABLE_NAME = "favorites";
  public static final class MovieColumns implements BaseColumns {
    public static String MOVIE_ID              = "movie_id";
    public static String TITLE                 = "title";
    public static String OVERVIEW              = "overview";
    public static String POSTER                = "poster";
    public static String RELEASE_DATE          = "release_date";
    public static String TAGLINE               = "tagline";
    public static String ORIGINAL_LANGUAGE     = "original_language";
    public static String ORIGINAL_TITLE        = "original_title";
    public static String VOTE_AVERAGE          = "vote_average";
    public static String PRODUCTION_COMPANIES  = "production_companies";
  }

  public static final String AUTHORITY = "xyz.winardiaris.android.moviedbwidget";
  public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
      .authority(AUTHORITY)
      .appendPath(TABLE_NAME)
      .build();

  public static String getColumnString(Cursor cursor, String columnname) {
    return cursor.getString(cursor.getColumnIndexOrThrow(columnname));
  }

  public static int getColumnInt(Cursor cursor, String columnname) {
    return cursor.getInt(cursor.getColumnIndexOrThrow(columnname));
  }

  public static long getColumnLong(Cursor cursor, String columnname) {
    return cursor.getLong(cursor.getColumnIndexOrThrow(columnname));
  }
}
