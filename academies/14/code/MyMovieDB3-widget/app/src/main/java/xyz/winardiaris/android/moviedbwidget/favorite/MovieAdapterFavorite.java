package xyz.winardiaris.android.moviedbwidget.favorite;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import xyz.winardiaris.android.moviedbwidget.BuildConfig;
import xyz.winardiaris.android.moviedbwidget.model.Movie;
import xyz.winardiaris.android.moviedbwidget.R;
import xyz.winardiaris.android.moviedbwidget.utils.CustomOnItemClickListener;
import xyz.winardiaris.android.moviedbwidget.utils.Utils;

/**
 * Created by winardiaris on 10/11/17.
 */

public class MovieAdapterFavorite extends RecyclerView.Adapter<MovieAdapterFavorite.CardViewViewHolder> {
  private Cursor listMovie;
  private Context context;
  private Utils utils;
  private static  String LIMIT_OVERVIEW = BuildConfig.LIMIT_OVERVIEW;
  private static ClickListener clickListener;

  MovieAdapterFavorite(Context context) {
    this.context = context;
  }

  private Cursor getListMovie() {
    return listMovie;
  }

  public void setListMovie(Cursor listMovie) {
    this.listMovie = listMovie;
  }

  @Override
  public CardViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_movie_favorite, parent, false);
    return new CardViewViewHolder(view);
  }

  @Override
  public void onBindViewHolder(CardViewViewHolder holder, int position) {
    Movie m = getItem(position);
    utils = new Utils();

    Glide.with(context)
        .load(m.getPoster())
        .override(300,400)
        .into(holder.moviePoster);

    holder.movieTitle.setText(m.getTitle());
    holder.movieOverview.setText(utils.getSafeSubstring(m.getOverview(), Integer.valueOf(MovieAdapterFavorite.LIMIT_OVERVIEW)));
    holder.movieReleaseDate.setText(utils.humanReadableDate(m.getRelease_date()));

    holder.buttonDelete.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
      @Override
      public void onItemClicked(View view, int position) {
        Toast.makeText(context, context.getResources().getString(R.string.delete) + " " + getItem(position).getTitle(), Toast.LENGTH_SHORT).show();
        utils.setDelete(context, getItem(position));
      }
    }));

  }

  private Movie getItem(int position){
    if (!listMovie.moveToPosition(position)) {
      throw new IllegalStateException("Position invalid");
    }
    return new Movie(listMovie);
  }


  @Override
  public int getItemCount() {
    if (listMovie == null) return 0;
    return listMovie.getCount();
  }

  public class CardViewViewHolder extends RecyclerView.ViewHolder
  implements View.OnClickListener {
    ImageView moviePoster;
    TextView movieTitle;
    TextView movieOverview;
    TextView movieReleaseDate;
    Button buttonDelete;

    public CardViewViewHolder(View itemView) {
      super(itemView);

      moviePoster       = itemView.findViewById(R.id.movie_poster);
      movieTitle        = itemView.findViewById(R.id.movie_title);
      movieOverview     = itemView.findViewById(R.id.movie_overview);
      movieReleaseDate  = itemView.findViewById(R.id.movie_release_date);
      buttonDelete      = itemView.findViewById(R.id.btn_delete);

      itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
      clickListener.onItemClick(getAdapterPosition(), v);
    }
  }

  public void setOnItemClickListener(ClickListener clickListener) {
    MovieAdapterFavorite.clickListener = clickListener;
  }

  public interface ClickListener {
    void onItemClick(int position, View v);
  }
}
