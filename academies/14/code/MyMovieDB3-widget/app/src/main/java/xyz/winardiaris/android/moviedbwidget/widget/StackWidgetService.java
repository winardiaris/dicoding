package xyz.winardiaris.android.moviedbwidget.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by winardiaris on 03/12/17.
 */

public class StackWidgetService extends RemoteViewsService {
  @Override
  public RemoteViewsFactory onGetViewFactory(Intent intent) {
    return new StackWidgetFactory(this.getApplicationContext(), intent);
  }
}
