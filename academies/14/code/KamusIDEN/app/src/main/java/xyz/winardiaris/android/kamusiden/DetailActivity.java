package xyz.winardiaris.android.kamusiden;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

import xyz.winardiaris.android.kamusiden.databases.EnIdHelper;
import xyz.winardiaris.android.kamusiden.databases.EnIdModel;
import xyz.winardiaris.android.kamusiden.databases.IdEnHelper;
import xyz.winardiaris.android.kamusiden.databases.IdEnModel;

public class DetailActivity extends AppCompatActivity {
  private static final String EXTRAS_KATA = "EXTRAS_KATA";
  private static final String EXTRAS_KET = "EXTRAS_KET";
  private static final String TAG = DetailActivity.class.getSimpleName();
  TextView tvKata;
  TextView tvKet;
  String kata;
  String ket;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    Intent intent = getIntent();

    tvKata = findViewById(R.id.tv_kata);
    tvKet = findViewById(R.id.tv_ket);
    kata = intent.getStringExtra(EXTRAS_KATA);
    ket = intent.getStringExtra(EXTRAS_KET);


    tvKata.setText(kata);
    tvKet.setText(ket);

  }
}
