package xyz.winardiaris.android.kamusiden.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static xyz.winardiaris.android.kamusiden.databases.IdEnContract.TABLE_NAME;
import static xyz.winardiaris.android.kamusiden.databases.IdEnContract.IdEnColoumns.KATA;
import static xyz.winardiaris.android.kamusiden.databases.IdEnContract.IdEnColoumns.KET;

/**
 * Created by winardiaris on 15/11/17.
 */

public class IdEnHelper {
  private Context context;
  private DatabaseHelper dataBaseHelper;

  private SQLiteDatabase database;
  private String limits = null;
  private String searchs = null;

  public IdEnHelper(Context context){
    this.context = context;
  }

  public IdEnHelper open() throws SQLException {
    dataBaseHelper = new DatabaseHelper(context);
    database = dataBaseHelper.getWritableDatabase();
    return this;
  }

  public void close(){
    dataBaseHelper.close();
  }

  public ArrayList<IdEnModel> getDataByKata(String search) {
    Cursor cursor = database.query(TABLE_NAME, null, KATA + " LIKE ?",
        new String[]{search}, null, null, _ID + " ASC", limits);

    cursor.moveToFirst();

    ArrayList<IdEnModel> arrayList = new ArrayList<>();
    IdEnModel model;
    if(cursor.getCount() > 0) {
      do {
        model = new IdEnModel();
        model.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        model.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
        model.setKet(cursor.getString(cursor.getColumnIndexOrThrow(KET)));

        arrayList.add(model);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }

  public ArrayList<IdEnModel> getDataById(int id) {

    Cursor cursor = database.query(TABLE_NAME, null, _ID + " LIKE ?",
        new String[]{String.valueOf(id)}, null, null, _ID + " ASC", limits);

    cursor.moveToFirst();

    ArrayList<IdEnModel> arrayList = new ArrayList<>();
    IdEnModel model;
    if(cursor.getCount() > 0) {
      do {
        model = new IdEnModel();
        model.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        model.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
        model.setKet(cursor.getString(cursor.getColumnIndexOrThrow(KET)));

        arrayList.add(model);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }

  public ArrayList<IdEnModel> getData(String search, int limit) {
    String selection = null;
    String[] selectionArgs = null;
    if(limit > 0) {
      limits = String.valueOf(limit);
    }

    if(search != null) {
      selection = KATA + " LIKE ?";
      selectionArgs = new String[]{search};
    }

    String result = "";
    Cursor cursor = database.query(TABLE_NAME, null, selection,
        selectionArgs, null, null, _ID + " ASC", limits);

    cursor.moveToFirst();

    ArrayList<IdEnModel> arrayList = new ArrayList<>();
    IdEnModel model;
    if(cursor.getCount() > 0) {
      do {
        model = new IdEnModel();
        model.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        model.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
        model.setKet(cursor.getString(cursor.getColumnIndexOrThrow(KET)));

        arrayList.add(model);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }

  public long insert(IdEnModel model) {
    ContentValues init = new ContentValues();
    init.put(KATA, model.getKata());
    init.put(KET, model.getKet());
    return database.insert(TABLE_NAME, null, init);
  }

  public void beginTransaction(){
   database.beginTransaction();
  }

  public void setTransactionSuccess(){
    database.setTransactionSuccessful();
  }

  public void endTransaction(){
    database.endTransaction();
  }

  public void insertTransaction(IdEnModel model) {
    String sql = "INSERT INTO " + TABLE_NAME + "( " + KATA + ", " + KET + ") VALUES(?, ?)";
    SQLiteStatement statement = database.compileStatement(sql);
    statement.bindString(1, model.getKata());
    statement.bindString(2, model.getKet());
    statement.execute();
    statement.clearBindings();
  }

  public int update(IdEnModel model) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(KATA, model.getKata());
    contentValues.put(KET, model.getKata());

    return database.update(TABLE_NAME, contentValues, _ID + "= '" + model.getId() + "' ", null);
  }

  public int delete(int id) {
    return database.delete(TABLE_NAME, _ID + "=' " + id + "'", null);
  }

}
