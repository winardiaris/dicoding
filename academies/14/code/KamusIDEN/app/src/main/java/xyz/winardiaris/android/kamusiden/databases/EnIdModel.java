package xyz.winardiaris.android.kamusiden.databases;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winardiaris on 15/11/17.
 */

public class EnIdModel implements Parcelable {
  private int id;
  private String kata;
  private String ket;

  public EnIdModel() {

  }

  public EnIdModel(String kata, String ket) {
    this.kata = kata;
    this.ket = ket;
  }

  public EnIdModel(int id, String kata, String ket) {
    this.id = id;
    this.kata = kata;
    this.ket = ket;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getKata() {
    return kata;
  }

  public void setKata(String kata) {
    this.kata = kata;
  }

  public String getKet() {
    return ket;
  }

  public void setKet(String ket) {
    this.ket = ket;
  }

  protected EnIdModel(Parcel in) {
    id = in.readInt();
    kata = in.readString();
    ket = in.readString();
  }

  public static final Creator<EnIdModel> CREATOR = new Creator<EnIdModel>() {
    @Override
    public EnIdModel createFromParcel(Parcel in) {
      return new EnIdModel(in);
    }

    @Override
    public EnIdModel[] newArray(int size) {
      return new EnIdModel[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeString(kata);
    dest.writeString(ket);
  }
}
