package xyz.winardiaris.android.kamusiden;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import xyz.winardiaris.android.kamusiden.databases.IdEnModel;

/**
 * Created by winardiaris on 16/11/17.
 */

public class IdEnAdapter extends RecyclerView.Adapter<IdEnAdapter.ViewViewHolder> {
  private Context context;
  private ArrayList<IdEnModel> listKamus;
  private ArrayList<IdEnModel> filterList;
  private LayoutInflater mInflater;

  public IdEnAdapter(Context context) {
    this.context = context;

    mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  public ArrayList<IdEnModel> getData() {
    return listKamus;
  }

  public void setData(ArrayList<IdEnModel> listKamus) {
    this.listKamus = listKamus;
    this.filterList = new ArrayList<IdEnModel>();
    this.filterList.addAll(this.listKamus);
    notifyDataSetChanged();
  }

  @Override
  public ViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
    return new ViewViewHolder(itemRow);
  }

  @Override
  public void onBindViewHolder(ViewViewHolder holder, int position) {
    IdEnModel listItem = filterList.get(position);

    holder.tvKata.setText(listItem.getKata());
    holder.tvKet.setText(listItem.getKet());
    holder.rlItem.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
      @Override
      public void onItemClicked(View view, int position) {
        Intent intent = new Intent(context, DetailActivity.class);

        String kata = String.valueOf(filterList.get(position).getKata());
        String ket = String.valueOf(filterList.get(position).getKet());
        intent.putExtra("EXTRAS_KATA", kata);
        intent.putExtra("EXTRAS_KET", ket);
        context.startActivity(intent);
      }
    }));
  }

  @Override
  public int getItemCount() {
    return (null != filterList ? filterList.size() : 0);
  }

  @Override
  public int getItemViewType(int position) {
    return 0;
  }
  @Override
  public long getItemId(int position) {
    return position;
  }

  public void filter(final String text) {

    new Thread(new Runnable() {
      @Override
      public void run() {

        filterList.clear();
        if (TextUtils.isEmpty(text)) {
          filterList.addAll(listKamus);
        } else {
          for (IdEnModel item : listKamus) {
            if (item.getKata().toLowerCase().contains(text.toLowerCase()) ||
                item.getKet().toLowerCase().contains(text.toLowerCase())) {
              filterList.add(item);
            }
          }
        }

        ((Activity) context).runOnUiThread(new Runnable() {
          @Override
          public void run() {
            notifyDataSetChanged();
          }
        });

      }
    }).start();

  }

  public class ViewViewHolder extends RecyclerView.ViewHolder {
    TextView tvKata;
    TextView tvKet;
    RelativeLayout rlItem;
    public ViewViewHolder(View itemView) {
      super(itemView);

      tvKata  = itemView.findViewById(R.id.tv_kata);
      tvKet   = itemView.findViewById(R.id.tv_ket);
      rlItem  = itemView.findViewById(R.id.rl_item);
    }
  }

}
