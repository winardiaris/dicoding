package xyz.winardiaris.android.kamusiden;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import java.util.ArrayList;

import xyz.winardiaris.android.kamusiden.databases.EnIdHelper;
import xyz.winardiaris.android.kamusiden.databases.EnIdModel;

public class EnIdFragment extends Fragment implements
    SearchView.OnQueryTextListener {
  private static final String TAG = EnIdFragment.class.getSimpleName() ;

  RecyclerView recyclerView;
  SearchView searchView;
  EnIdAdapter adapter;
  EnIdHelper helper;

  public EnIdFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    helper = new EnIdHelper(getActivity());
    adapter = new EnIdAdapter(getActivity());
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view =  inflater.inflate(R.layout.fragment_en_id, container, false);
    searchView = view.findViewById(R.id.search_view);

    recyclerView = view.findViewById(R.id.rv_en_id);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    helper.open();
    final ArrayList<EnIdModel> models = helper.getData(0);
    helper.close();
    adapter.setData(models);
    recyclerView.setAdapter(adapter);

    setupSearchView();

    return view;
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    adapter.filter(newText);
    return true;
  }

  private void setupSearchView() {
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setSubmitButtonEnabled(true);
    searchView.setQueryHint(getContext().getResources().getString(R.string.search));
  }
}
