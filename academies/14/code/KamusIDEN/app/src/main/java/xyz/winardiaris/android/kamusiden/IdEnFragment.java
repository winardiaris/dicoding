package xyz.winardiaris.android.kamusiden;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;

import java.util.ArrayList;

import xyz.winardiaris.android.kamusiden.databases.IdEnHelper;
import xyz.winardiaris.android.kamusiden.databases.IdEnModel;

public class IdEnFragment extends Fragment implements
    SearchView.OnQueryTextListener{
  private static final String TAG = IdEnFragment.class.getSimpleName() ;

  RecyclerView recyclerView;
  SearchView searchView;
  IdEnAdapter adapter;
  IdEnHelper helper;
  String search = null;

  public IdEnFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    helper = new IdEnHelper(getActivity());
    adapter = new IdEnAdapter(getActivity());
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view =  inflater.inflate(R.layout.fragment_id_en, container, false);
    searchView = view.findViewById(R.id.search_view);


    recyclerView = view.findViewById(R.id.rv_id_en);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    adapter.notifyDataSetChanged();

    helper.open();
    final ArrayList<IdEnModel> models = helper.getData(search,0);
    helper.close();
    adapter.setData(models);
    recyclerView.setAdapter(adapter);

    setupSearchView();

    return view;
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    adapter.filter(newText);
    return true;
  }

  private void setupSearchView() {
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setSubmitButtonEnabled(true);
    searchView.setQueryHint(getContext().getResources().getString(R.string.search));
  }
}
