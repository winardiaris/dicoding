package xyz.winardiaris.android.kamusiden.databases;

import android.provider.BaseColumns;

/**
 * Created by winardiaris on 15/11/17.
 */

public class IdEnContract {
  static String TABLE_NAME = "table_id_en";

  public static final class IdEnColoumns implements BaseColumns {
    public static String KATA  = "kata";
    public static String KET   = "ket";
  }
}
