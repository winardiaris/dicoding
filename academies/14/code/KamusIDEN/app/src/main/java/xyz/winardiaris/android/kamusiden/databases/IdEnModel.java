package xyz.winardiaris.android.kamusiden.databases;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by winardiaris on 15/11/17.
 */

public class IdEnModel implements Parcelable {
  private int id;
  private String kata;
  private String ket;

  public IdEnModel() {

  }

  public IdEnModel(String kata, String ket) {
    this.kata = kata;
    this.ket = ket;
  }

  public IdEnModel(int id, String kata, String ket) {
    this.id = id;
    this.kata = kata;
    this.ket = ket;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getKata() {
    return kata;
  }

  public void setKata(String kata) {
    this.kata = kata;
  }

  public String getKet() {
    return ket;
  }

  public void setKet(String ket) {
    this.ket = ket;
  }

  protected IdEnModel(Parcel in) {
    id = in.readInt();
    kata = in.readString();
    ket = in.readString();
  }

  public static final Creator<IdEnModel> CREATOR = new Creator<IdEnModel>() {
    @Override
    public IdEnModel createFromParcel(Parcel in) {
      return new IdEnModel(in);
    }

    @Override
    public IdEnModel[] newArray(int size) {
      return new IdEnModel[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeString(kata);
    dest.writeString(ket);
  }
}
