package xyz.winardiaris.android.kamusiden.databases;

import android.provider.BaseColumns;

/**
 * Created by winardiaris on 15/11/17.
 */

public class EnIdContract {
  static String TABLE_NAME = "table_en_id";

  public static final class EnIdColoumns implements BaseColumns {
    public static String KATA  = "kata";
    public static String KET   = "ket";
  }
}
