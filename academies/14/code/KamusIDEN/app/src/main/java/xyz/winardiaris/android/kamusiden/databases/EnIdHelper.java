package xyz.winardiaris.android.kamusiden.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static xyz.winardiaris.android.kamusiden.databases.EnIdContract.TABLE_NAME;
import static xyz.winardiaris.android.kamusiden.databases.EnIdContract.EnIdColoumns.KATA;
import static xyz.winardiaris.android.kamusiden.databases.EnIdContract.EnIdColoumns.KET;

/**
 * Created by winardiaris on 15/11/17.
 */

public class EnIdHelper {
  private Context context;
  private DatabaseHelper dataBaseHelper;

  private SQLiteDatabase database;
  private String limits = null;

  public EnIdHelper(Context context){
    this.context = context;
  }

  public EnIdHelper open() throws SQLException {
    dataBaseHelper = new DatabaseHelper(context);
    database = dataBaseHelper.getWritableDatabase();
    return this;
  }

  public void close(){
    dataBaseHelper.close();
  }

  public ArrayList<EnIdModel> getDataByKata(String search) {
    String result = "";
    Cursor cursor = database.query(TABLE_NAME, null, KATA + " LIKE ?",
        new String[]{search}, null, null, _ID + " ASC", null);

    cursor.moveToFirst();

    ArrayList<EnIdModel> arrayList = new ArrayList<>();
    EnIdModel model;
    if(cursor.getCount() > 0) {
      do {
        model = new EnIdModel();
        model.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        model.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
        model.setKet(cursor.getString(cursor.getColumnIndexOrThrow(KET)));

        arrayList.add(model);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }

  public ArrayList<EnIdModel> getDataById(int id) {

    Cursor cursor = database.query(EnIdContract.TABLE_NAME, null, _ID + " LIKE ?",
        new String[]{String.valueOf(id)}, null, null, _ID + " ASC", limits);

    cursor.moveToFirst();

    ArrayList<EnIdModel> arrayList = new ArrayList<>();
    EnIdModel model;
    if(cursor.getCount() > 0) {
      do {
        model = new EnIdModel();
        model.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        model.setKata(cursor.getString(cursor.getColumnIndexOrThrow(EnIdContract.EnIdColoumns.KATA)));
        model.setKet(cursor.getString(cursor.getColumnIndexOrThrow(EnIdContract.EnIdColoumns.KET)));

        arrayList.add(model);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }
  
  public ArrayList<EnIdModel> getData(int limit) {
    if(limit > 0) {
      limits = String.valueOf(limit);
    }
    String result = "";
    Cursor cursor = database.query(TABLE_NAME, null, null,
        null, null, null, _ID + " ASC", limits);

    cursor.moveToFirst();

    ArrayList<EnIdModel> arrayList = new ArrayList<>();
    EnIdModel model;
    if(cursor.getCount() > 0) {
      do {
        model = new EnIdModel();
        model.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
        model.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
        model.setKet(cursor.getString(cursor.getColumnIndexOrThrow(KET)));

        arrayList.add(model);
        cursor.moveToNext();

      } while (!cursor.isAfterLast());
    }
    cursor.close();
    return arrayList;
  }

  public long insert(EnIdModel model) {
    ContentValues init = new ContentValues();
    init.put(KATA, model.getKata());
    init.put(KET, model.getKet());
    return database.insert(TABLE_NAME, null, init);
  }

  public void beginTransaction(){
    database.beginTransaction();
  }

  public void setTransactionSuccess(){
    database.setTransactionSuccessful();
  }

  public void endTransaction(){
    database.endTransaction();
  }

  public void insertTransaction(EnIdModel model) {
    String sql = "INSERT INTO " + TABLE_NAME + "( " + KATA + ", " + KET + ") VALUES(?, ?)";
    SQLiteStatement statement = database.compileStatement(sql);
    statement.bindString(1, model.getKata());
    statement.bindString(2, model.getKet());
    statement.execute();
    statement.clearBindings();
  }

  public int update(EnIdModel model) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(KATA, model.getKata());
    contentValues.put(KET, model.getKata());

    return database.update(TABLE_NAME, contentValues, _ID + "= '" + model.getId() + "' ", null);
  }

  public int delete(int id) {
    return database.delete(TABLE_NAME, _ID + "=' " + id + "'", null);
  }

}
