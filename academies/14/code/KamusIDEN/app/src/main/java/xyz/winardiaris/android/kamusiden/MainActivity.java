package xyz.winardiaris.android.kamusiden;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import xyz.winardiaris.android.kamusiden.databases.EnIdHelper;
import xyz.winardiaris.android.kamusiden.databases.EnIdModel;
import xyz.winardiaris.android.kamusiden.databases.IdEnHelper;
import xyz.winardiaris.android.kamusiden.databases.IdEnModel;

public class MainActivity extends AppCompatActivity {
  ProgressBar progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    progressBar = (ProgressBar) findViewById(R.id.progress_bar);

    new loadData().execute();
  }

  private class loadData extends AsyncTask<Void, Integer, Void> {
    final String TAG = loadData.class.getSimpleName();
    IdEnHelper helper1;
    EnIdHelper helper2;
    AppPreference preference;

    double progress;
    double maxprogress = 100;

    @Override
    protected void onPreExecute() {
      helper1 = new IdEnHelper(MainActivity.this);
      helper2 = new EnIdHelper(MainActivity.this);
      preference = new AppPreference(MainActivity.this);
    }

    @Override
    protected Void doInBackground(Void... voids) {
      Boolean firstRun = preference.getFirstRun();
      if(firstRun) {
        //todo progress
        ArrayList<IdEnModel> model1 = preLoadRawIdEn();
        ArrayList<EnIdModel> model2 = preLoadRawEnId();

        progress = 10;
        publishProgress((int) progress);
        Double progressMaxInsert = 80.0;
        Double progressDiff = (progressMaxInsert - progress) / (model1.size() + model2.size());

        helper1.open();
        helper1.beginTransaction();
        try {
          for (IdEnModel model : model1) {
            helper1.insertTransaction(model);
            progress += progressDiff;
            publishProgress((int)progress);
          }
          helper1.setTransactionSuccess();
        } catch (Exception e) {
          Log.e(TAG, "doInBackground: ", e);
        }
        helper1.endTransaction();
        helper1.close();

        helper2.open();
        helper2.beginTransaction();
        try {
          for (EnIdModel model : model2) {
            helper2.insertTransaction(model);
            progress += progressDiff;
            publishProgress((int)progress);
          }
          helper2.setTransactionSuccess();
        } catch (Exception e) {
          Log.e(TAG, "doInBackground: ", e);
        }
        helper2.endTransaction();
        helper2.close();

        preference.setFirstRun(false);
      } else {
        try {
          synchronized (this) {
            this.wait(2000);

            publishProgress(50);

            this.wait(2000);
            publishProgress((int) maxprogress);
          }
        } catch (Exception e) {
        }

      }
      return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      Intent i = new Intent(MainActivity.this, KamusActivity.class);
      startActivity(i);
      finish();
    }
  }

  public ArrayList<IdEnModel> preLoadRawIdEn() {
    ArrayList<IdEnModel> arrayList = new ArrayList<>();
    String line = null;
    BufferedReader reader;
    try {
      Resources res = getResources();
      InputStream raw = res.openRawResource(R.raw.indonesia_english);

      reader = new BufferedReader(new InputStreamReader(raw));
      int count = 0;
      do {
          line = reader.readLine();
          String[] split = line.split("\t");

          IdEnModel model;

          model = new IdEnModel(split[0], split[1]);
          arrayList.add(model);
          count++;
      } while ( line != null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return arrayList;
  }

  public ArrayList<EnIdModel> preLoadRawEnId() {
    ArrayList<EnIdModel> arrayList = new ArrayList<>();
    String line = null;
    BufferedReader reader;
    try {
      Resources res = getResources();
      InputStream raw = res.openRawResource(R.raw.english_indonesia);

      reader = new BufferedReader(new InputStreamReader(raw));
      int count = 0;
      do {
        line = reader.readLine();
        String[] split = line.split("\t");

        EnIdModel model;

        model = new EnIdModel(split[0], split[1]);
        arrayList.add(model);
        count++;
      } while ( line != null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return arrayList;
  }
}
