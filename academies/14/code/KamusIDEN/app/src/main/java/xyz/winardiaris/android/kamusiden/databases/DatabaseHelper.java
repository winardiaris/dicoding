package xyz.winardiaris.android.kamusiden.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static xyz.winardiaris.android.kamusiden.databases.EnIdContract.EnIdColoumns;
import static xyz.winardiaris.android.kamusiden.databases.IdEnContract.IdEnColoumns;

/**
 * Created by winardiaris on 15/11/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
  private static String DATABASE_NAME = "db_kamus";

  private static final int DATABASE_VERSION = 1;

  public static String CREATE_TABLE_ID_EN = "create table " + IdEnContract.TABLE_NAME +
      " (" + _ID + " integer primary key autoincrement, " +
      IdEnColoumns.KATA + " text not null, " +
      IdEnColoumns.KET  + " text not null);";

  public static String CREATE_TABLE_EN_ID = "create table " + EnIdContract.TABLE_NAME +
      " (" + _ID + " integer primary key autoincrement, " +
      EnIdColoumns.KATA + " text not null, " +
      EnIdColoumns.KET  + " text not null);";

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }


  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TABLE_ID_EN);
    db.execSQL(CREATE_TABLE_EN_ID);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_ID_EN);
    db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_EN_ID);
    onCreate(db);
  }
}
