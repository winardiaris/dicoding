package xyz.winardiaris.android.myflexiblefragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailCategoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailCategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailCategoryFragment extends Fragment implements View.OnClickListener {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  private TextView tvCategoryName;
  private TextView tvCategoryDescription;
  private Button btnProfile;
  private Button btnShowDialog;
  public static String EXTRA_NAME = "extra_name";
  private String description;

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;

  private OnFragmentInteractionListener mListener;

  public DetailCategoryFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param param1 Parameter 1.
   * @param param2 Parameter 2.
   * @return A new instance of fragment DetailCategoryFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static DetailCategoryFragment newInstance(String param1, String param2) {
    DetailCategoryFragment fragment = new DetailCategoryFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view =  inflater.inflate(R.layout.fragment_detail_category, container, false);
    tvCategoryName = (TextView)view.findViewById(R.id.tv_category_name);
    tvCategoryDescription = (TextView)view.findViewById(R.id.tv_category_description);
    btnProfile = (Button)view.findViewById(R.id.btn_profile);
    btnProfile.setOnClickListener(this);
    btnShowDialog = (Button)view.findViewById(R.id.btn_show_dialog);
    btnShowDialog.setOnClickListener(this);

    String categoryName = getArguments().getString(EXTRA_NAME);
    tvCategoryName.setText(categoryName);
    tvCategoryDescription.setText(getDescription());
    return view;
  }

  // TODO: Rename method, update argument and hook method into UI event
  public void onButtonPressed(Uri uri) {
    if (mListener != null) {
      mListener.onFragmentInteraction(uri);
    }
  }

//  @Override
//  public void onAttach(Context context) {
//    super.onAttach(context);
//    if (context instanceof OnFragmentInteractionListener) {
//      mListener = (OnFragmentInteractionListener) context;
//    } else {
//      throw new RuntimeException(context.toString()
//          + " must implement OnFragmentInteractionListener");
//    }
//  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.btn_profile:
        break;
      case R.id.btn_show_dialog:
        OptionDialogFragment optionDialogFragment = new OptionDialogFragment();
        optionDialogFragment.setOnOptionDialogListener(new OptionDialogFragment.OnOptionDialogListener() {
          @Override
          public void onOptionChoosen(String text) {
            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
          }
        });
        FragmentManager fragmentManager = getChildFragmentManager(); optionDialogFragment.show(fragmentManager,OptionDialogFragment.class.getSimpleName());
        break;
    }
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
  }

  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }
}
