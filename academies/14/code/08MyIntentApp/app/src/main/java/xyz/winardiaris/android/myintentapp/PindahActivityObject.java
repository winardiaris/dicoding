package xyz.winardiaris.android.myintentapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class PindahActivityObject extends AppCompatActivity {
  public static String EXTRA_PERSON = "extra_person";
  private TextView tvObject;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pindah_object);

    tvObject = (TextView)findViewById(R.id.tv_object_diterima);
    Person mPerson = getIntent().getParcelableExtra(EXTRA_PERSON);
    String text = "Nama : " + mPerson.getName() + ", Email : " + mPerson.getEmail() + ", Umur : "
        + mPerson.getAge() + ", Lokasi : "+mPerson.getCity();
    tvObject.setText(text);
  }
}
