package xyz.winardiaris.android.myintentapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class PindahActivityData extends AppCompatActivity {
  public static String EXTRA_UMUR = "extra_umur";
  public static String EXTRA_NAMA = "extra_nama";
  private TextView tvDataDiterima;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pindah_data);
    tvDataDiterima = (TextView)findViewById(R.id.tv_data_diterima);

    String nama = getIntent().getStringExtra(EXTRA_NAMA);
    int umur = getIntent().getIntExtra(EXTRA_UMUR, 0);

    String text = "Nama : " + nama + ", Umur : "+ umur;
    tvDataDiterima.setText(text);
  }
}
