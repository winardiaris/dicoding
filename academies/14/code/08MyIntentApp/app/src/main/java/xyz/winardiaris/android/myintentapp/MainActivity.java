package xyz.winardiaris.android.myintentapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
  private Button btnPindahActivity;
  private Button btnPindahActivityData;
  private Button btnPindahActivityObject;
  private Button btnDialPhone;
  private Button btnMoveForResult;
  private TextView tvResult;
  private int REQUEST_CODE = 100;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnPindahActivity = (Button)findViewById(R.id.btn_pindah_activity);
    btnPindahActivity.setOnClickListener(this);
    btnPindahActivityData = (Button)findViewById(R.id.btn_pindah_activity_data);
    btnPindahActivityData.setOnClickListener(this);
    btnPindahActivityObject = (Button)findViewById(R.id.btn_pindah_activity_object);
    btnPindahActivityObject.setOnClickListener(this);
    btnDialPhone = (Button)findViewById(R.id.btn_dial);
    btnDialPhone.setOnClickListener(this);
    btnMoveForResult = (Button)findViewById(R.id.btn_pindah_activity_result);
    btnMoveForResult.setOnClickListener(this);
    tvResult = (TextView)findViewById(R.id.tv_hasil);
  }


  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.btn_pindah_activity:
        Intent pindahIntent = new Intent(MainActivity.this, PindahActivity.class);
        startActivity(pindahIntent);
        break;

      case R.id.btn_pindah_activity_data:
        Intent pindahIntentData = new Intent(MainActivity.this, PindahActivityData.class);
        pindahIntentData.putExtra(PindahActivityData.EXTRA_NAMA, "Aris Winardi");
        pindahIntentData.putExtra(PindahActivityData.EXTRA_UMUR, 24);
        startActivity(pindahIntentData);
        break;

      case R.id.btn_pindah_activity_object:
        Person mPerson = new Person();
        mPerson.setName("Aris Winardi");
        mPerson.setAge(17);
        mPerson.setEmail("winardiaris@di.blankon.in");
        mPerson.setCity("Bogor");
        Intent moveWithObjectIntent = new Intent(MainActivity.this, PindahActivityObject.class);
        moveWithObjectIntent.putExtra(PindahActivityObject.EXTRA_PERSON, mPerson);
        startActivity(moveWithObjectIntent);
        break;

      case R.id.btn_dial:
        String phoneNumber = "081210841382";
        Intent dialPhoneIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phoneNumber));
        startActivity(dialPhoneIntent);
        break;

      case R.id.btn_pindah_activity_result:
        Intent moveForResultIntent = new Intent(MainActivity.this, PindahActivityResult.class);
        startActivityForResult(moveForResultIntent, REQUEST_CODE);
        break;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_CODE){
      if (resultCode == PindahActivityResult.RESULT_CODE){
        int selectedValue = data.getIntExtra(PindahActivityResult.EXTRA_SELECTED_VALUE, 0);
        tvResult.setText("Hasil : "+selectedValue);
      }
    }
  }
}
