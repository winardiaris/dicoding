package xyz.winardiaris.android.mymultilanguageapp;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
  TextView tvHello, tvPlural, tvXliff;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    int pokeCount = 3;
    int songCount = 5;

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    tvHello   = findViewById(R.id.tv_hello);
    tvPlural  = findViewById(R.id.tv_plural);
    tvXliff   = findViewById(R.id.tv_xliff);

    String hello = String.format(getResources().getString(R.string.hello_world), "Narenda Wicaksono", pokeCount, "Yoza Aprilio");
    String pluralText = getResources().getQuantityString(R.plurals.numberOfSongsAvailable, songCount, songCount);

    tvHello.setText(hello);
    tvPlural.setText(pluralText);
    tvXliff.setText(getResources().getString(R.string.app_homeurl));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_change_settings){
      Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
      startActivity(mIntent);
    }
    return super.onOptionsItemSelected(item);
  }
}
