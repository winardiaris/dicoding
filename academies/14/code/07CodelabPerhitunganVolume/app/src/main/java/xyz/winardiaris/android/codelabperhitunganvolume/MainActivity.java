package xyz.winardiaris.android.codelabperhitunganvolume;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MainView {
  private EditText edtTinggi, edtLebar, edtPanjang;
  private Button btnHitung;
  private TextView tvHasil;
  private static final String STATE_HASIL = "state_hasil";

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putString(STATE_HASIL, tvHasil.getText().toString());
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    edtTinggi = (EditText) findViewById(R.id.edt_tinggi);
    edtLebar = (EditText) findViewById(R.id.edt_lebar);
    edtPanjang = (EditText) findViewById(R.id.edt_panjang);
    tvHasil = (TextView) findViewById(R.id.tv_hasil);
    btnHitung = (Button) findViewById(R.id.btn_hitung);
//    btnHitung.setOnClickListener(this);

    final MainPresenter presenter = new MainPresenter(this);

    btnHitung.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        String tinggi = edtTinggi.getText().toString().trim();
        String panjang = edtPanjang.getText().toString().trim();
        String lebar = edtLebar.getText().toString().trim();
        boolean isEmptyField = false;

        if (TextUtils.isEmpty(tinggi)) {
          isEmptyField = true;
          edtTinggi.setError("tidak boleh kosong");
        }

        if (TextUtils.isEmpty(panjang)) {
          isEmptyField = true;
          edtTinggi.setError("tidak boleh kosong");
        }

        if (TextUtils.isEmpty(lebar)) {
          isEmptyField = true;
          edtTinggi.setError("tidak boleh kosong");
        }

        if (!isEmptyField) {
          double p = Double.parseDouble(panjang);
          double l = Double.parseDouble(lebar);
          double t = Double.parseDouble(tinggi);
          presenter.hitungVolume(p, l, t);
        }
      }
    });
  }
  @Override
  public void tampilVolume(MainModel model) {
    tvHasil.setText(String.valueOf(model.getVolume()));
  }
}
