package xyz.winardiaris.android.codelabperhitunganvolume;

/**
 * Created by winardiaris on 23/10/17.
 */

public class MainModel {
  private double volume;

  public MainModel(double volume) {
    this.volume = volume;
  }

  public double getVolume() {
    return volume;
  }

}
