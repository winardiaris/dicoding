package xyz.winardiaris.android.myrecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
  RecyclerView rvCategory;
  private ArrayList<President> list;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    rvCategory = findViewById(R.id.rv_category);
    rvCategory.setHasFixedSize(true);

    list = new ArrayList<>();
    list.addAll(PresidentData.getListData());
    showRecylerList();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    String title = null;
    switch (item.getItemId()){
      case R.id.action_list:
        title = "Mode List";
        showRecylerList();

      case R.id.action_grid:
        title = "Mode Grid";
        showRecyclerGrid();

      case R.id.action_cardview:
        title = "Mode Card";
        showRecyclerCard();
    }
    setActiobarTitle(title);
    return super.onOptionsItemSelected(item);
  }

  private void showRecylerList() {
    rvCategory.setLayoutManager(new LinearLayoutManager(this));
    ListPresidentAdapter listPresidentAdapter = new ListPresidentAdapter(this);
    listPresidentAdapter.setListPresident(list);
    rvCategory.setAdapter(listPresidentAdapter);
  }

  private void showRecyclerGrid() {
    rvCategory.setLayoutManager(new GridLayoutManager(this, 2));
    GridPresidentAdapter gridPresidentAdapter = new GridPresidentAdapter(this);
    gridPresidentAdapter.setListPresident(list);
    rvCategory.setAdapter(gridPresidentAdapter);
  }
  private void showRecyclerCard() {
    rvCategory.setLayoutManager(new LinearLayoutManager(this));
    CardViewPresidentAdapter cardViewPresidentAdapter = new CardViewPresidentAdapter(this);
    cardViewPresidentAdapter.setListPresident(list);
    rvCategory.setAdapter(cardViewPresidentAdapter);
  }

  private void setActiobarTitle(String t) {
    getSupportActionBar().setTitle(t);
  }
}
