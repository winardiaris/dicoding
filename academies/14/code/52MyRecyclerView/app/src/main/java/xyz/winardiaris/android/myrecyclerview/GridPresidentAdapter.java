package xyz.winardiaris.android.myrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by winardiaris on 02/11/17.
 */

public class GridPresidentAdapter extends RecyclerView.Adapter<GridPresidentAdapter.GridViewHolder> {
  private Context context;
  private ArrayList<President> listPresident;

  private ArrayList<President> getListPresident() {
    return listPresident;
  }

  void setListPresident(ArrayList<President> listPresident) {
    this.listPresident= listPresident;
  }

  GridPresidentAdapter (Context context) {
    this.context = context;
  }

  @Override
  public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_president, parent, false);
    return new GridViewHolder(view);
  }

  @Override
  public void onBindViewHolder(GridViewHolder holder, int position) {
    Glide.with(context)
        .load(getListPresident().get(position).getPhoto())
        .override(350,550)
        .into(holder.imgPhoto);
  }

  @Override
  public int getItemCount() {
    return getListPresident().size();
  }

  public class GridViewHolder extends RecyclerView.ViewHolder {
    ImageView imgPhoto;

    public GridViewHolder(View itemView) {
      super(itemView);
      imgPhoto = itemView.findViewById(R.id.img_item_photo);
    }
  }
}
